package me.ienze.Mech.inventory;

public enum EnumItem {

	RESOURCE(1, 0);

	private static EnumItem[] idMap = new EnumItem[127];

	private int id;
	private int maxAmount;

	private EnumItem(int id, int maxAmount) {
		this.id = id;
		this.maxAmount = maxAmount;
	}

	public int getId() {
		return id;
	}

	public int getMaxAmount() {
		return maxAmount;
	}

	static {
		for (EnumItem i : EnumItem.values()) {
			idMap[i.getId()] = i;
		}
	}

	public static EnumItem byId(int id) {
		return idMap[id];
	}
}
