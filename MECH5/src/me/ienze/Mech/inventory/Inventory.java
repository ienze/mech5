package me.ienze.Mech.inventory;

import java.util.ArrayList;

import me.ienze.Mech.common.InventoryOwner;
import me.ienze.Mech.data.value.FutureValueInteger;
import me.ienze.Mech.data.value.FutureValueItemsList;

import org.json.simple.JSONObject;

public class Inventory {

	private String id;
	private FutureValueInteger size;
	private FutureValueItemsList items;
	private InventoryOwner inventoryOwner;

	public Inventory(String id, InventoryOwner inventoryOwner) {
		this.id = id;
		this.size = new FutureValueInteger("inventory." + id + ".size");
		this.items = new FutureValueItemsList("inventory." + id + ".items", new ArrayList<Item>());
		this.inventoryOwner = inventoryOwner;
	}

	public Inventory(String id, int size, InventoryOwner inventoryOwner) {
		this(id, inventoryOwner);
		this.size.addValueChange(0, size);
	}

	public String getId() {
		return id;
	}

	public int getSize(long time) {
		return size.getValue(time);
	}

	public void setSize(long time, int value) {
		size.addValueChange(time, value);
	}

	public Item getItem(int slot, long time) {

		ArrayList<Item> inv = items.getValue(time);

		if (inv == null)
			return null;

		return slot < inv.size() ? inv.get(slot) : null;
	}

	public void setItem(int slot, Item item, long time) {
		this.items.createValue(time).add(slot, item);
		// TODO check this.. so it will work per slot.. (dont override with new
		// slor on higher time)
	}

	public ArrayList<Item> addItem(long time, Item... items) {
		ArrayList<Item> returnItems = new ArrayList<Item>();

		for (Item item : items) {
			returnItems.add(addItem(item, time));
		}

		return returnItems;
	}

	public Item addItem(Item item, long time) {

		for (int i = 0; i < getSize(time); i++) {

			Item slot = getItem(i, time);
			if (slot != null && slot.getType() == item.getType() && slot.getSubType() == item.getSubType()) {

				int max = EnumItem.byId(slot.getType()).getMaxAmount();
				if (slot.getAmount() < max) {
					int maxMove = max - slot.getAmount();
					int move = maxMove;
					if (move > item.getAmount()) {
						move = item.getAmount();
					}

					item.setAmount(item.getAmount() - move);
					slot.setAmount(slot.getAmount() + move);

					inventoryOwner.updateInventorySlot(i, time);

					if (item.getAmount() == 0) {
						return null;
					}
				}
			}
		}

		for (int i = 0; i < getSize(time); i++) {
			if (getItem(i, time) == null) {
				setItem(i, item, time);

				inventoryOwner.updateInventorySlot(i, time);

				return null;
			}
		}

		return item;
	}

	public boolean containsItem(byte id, byte subId, long time) {
		for (int i = 0; i < getSize(time); i++) {
			Item item = getItem(i, time);
			if (item != null && item.getType() == id && (subId < 0 || item.getSubType() == subId)) {
				return true;
			}
		}
		return false;
	}

	public int count(byte id, byte subId, long time) {
		int count = 0;
		for (int i = 0; i < getSize(time); i++) {
			Item item = getItem(i, time);
			if (item != null && item.getType() == id && (subId < 0 || item.getSubType() == subId)) {
				count += item.getAmount();
			}
		}
		return count;
	}

	public boolean removeItem(Item item, long time) {
		for (int i = 0; i < getSize(time); i++) {
			if (getItem(i, time) == item) {
				setItem(i, null, time);
				inventoryOwner.updateInventorySlot(i, time);
				return true;
			}
		}
		return false;
	}

	public boolean removeItem(byte id, byte subId, int amount, long time) {
		int amountToRemove = amount;

		for (int i = 0; i < getSize(time); i++) {
			Item item = getItem(i, time);
			if (item != null && item.getType() == id && (subId < 0 || item.getSubType() == subId)) {
				if (item.getAmount() > amountToRemove) {
					item.setAmount(item.getAmount() - amountToRemove);
					amountToRemove = 0;
					break;
				} else {
					amountToRemove -= item.getAmount();
					setItem(i, null, time);
				}
				inventoryOwner.updateInventorySlot(i, time);
			}
		}
		return amountToRemove == 0;
	}

	public void moveItem(int oldSlot, int newSlot, long time) {
		Item item = getItem(oldSlot, time);
		setItem(newSlot, item, time);
		setItem(oldSlot, null, time);
	}

	public FutureValueItemsList getItems() {
		return items;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getItemsJson(long time) {
		JSONObject out = new JSONObject();

		for (int i = 0; i < getSize(time); i++) {
			Item item = getItem(i, time);
			if (item != null) {
				out.put(i, item.getItemJson());
			}
		}

		return out;
	}
}
