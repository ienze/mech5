package me.ienze.Mech.inventory;

import org.json.simple.JSONObject;

public class Item {

	private byte type;
	private byte subType;
	private int amount;

	public Item(byte type, byte subType, int amount) {
		super();
		this.type = type;
		this.subType = subType;
		this.amount = amount;
	}

	public Item(int type, int subType, int amount) {
		super();
		this.type = (byte) type;
		this.subType = (byte) subType;
		this.amount = amount;
	}

	public byte getType() {
		return type;
	}

	public byte getSubType() {
		return subType;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getItemJson() {
		JSONObject json = new JSONObject();

		json.put("t", type);
		json.put("s", subType);
		json.put("a", amount);

		return json;
	}
}
