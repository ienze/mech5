package me.ienze.Mech.common;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginClassLoader<T> {

	protected ArrayList<T> loadPluginClasses(File questDir) {
		
		ArrayList<T> pluginClasses = new ArrayList<T>();
		
		if(questDir.exists() && questDir.isDirectory()) {
			
			for(File file : questDir.listFiles()) {
				
				if(file.getName().startsWith("."))
					continue;
				
				if(file.getName().endsWith(".jar")) {
					
					System.out.println("loading jar file.. ("+file.getName()+")");
					
					pluginClasses.addAll(loadPluginClassesJar(file.getPath()));
				}
			}
		}
		
		return pluginClasses;
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<T> loadPluginClassesJar(String pathToJar) {

		ArrayList<T> pluginClasses = new ArrayList<T>();
		
		try {
			JarFile jarFile = new JarFile(pathToJar);
			
			Enumeration<JarEntry> e = jarFile.entries();

	        URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
	        URLClassLoader cl = URLClassLoader.newInstance(urls);

	        while (e.hasMoreElements()) {
	        	
	            JarEntry je = (JarEntry) e.nextElement();
	            if(je.isDirectory() || !je.getName().endsWith(".class")){
	                continue;
	            }
	            
	            // -6 because of .class
	            String className = je.getName().substring(0, je.getName().length()-6);
	            className = className.replace('/', '.');
	            Class<?> questClass = cl.loadClass(className);

	            //if(questClass.getSuperclass() == T) { //impossible
	            	try {
	            		pluginClasses.add((T) questClass.newInstance());
	            	} catch (Exception e1) {
	            		e1.printStackTrace();
	            	}
	            //}
	        }
	        
	        jarFile.close();
	        
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
		return pluginClasses;
	}
	
}
