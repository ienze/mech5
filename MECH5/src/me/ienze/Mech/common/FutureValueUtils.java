package me.ienze.Mech.common;

import me.ienze.Mech.data.value.FutureValue;

public class FutureValueUtils {

	public static boolean doesChange(FutureValue<?> value, long timeFrom, long timeTo) {
		return !value.getValue(timeFrom).equals(value.getValue(timeTo));
	}
}
