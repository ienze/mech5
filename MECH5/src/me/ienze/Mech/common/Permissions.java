package me.ienze.Mech.common;

import me.ienze.Mech.player.Player;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Permissions {

	private JSONObject permissions;

	public Permissions(JSONObject permissions) {
		this.permissions = permissions;
	}

	public boolean hasPermission(Player player, String permission) {
		JSONArray perms = (JSONArray) permissions.get(player.getName());
		if (perms != null) {
			for (Object po : perms) {
				String perm = ((String) po);

				if (permission.equals(perm) || perm.endsWith("/*")) {
					if (permission.endsWith(perm.substring(0, perm.length() - 1))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public JSONObject getPermissions() {
		return permissions;
	}

	@SuppressWarnings("unchecked")
	public void addPermission(Player player, String permission) {
		JSONArray perms = (JSONArray) permissions.get(player.getName());
		if (perms == null) {
			perms = new JSONArray();
			permissions.put(player.getName(), perms);
		}

		perms.add(permission);
	}

	public void removePermission(Player player, String permission) {
		JSONArray perms = (JSONArray) permissions.get(player.getName());
		if (perms != null) {
			perms.remove(permission);
		}
	}
}
