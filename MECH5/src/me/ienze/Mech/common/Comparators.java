package me.ienze.Mech.common;

import java.util.Comparator;

import me.ienze.Mech.connection.Packet;

public class Comparators {

	public static class PacketComparator implements Comparator<Packet> {

		@Override
		public int compare(Packet p1, Packet p2) {
			return (p1.getPacketPriority() < p2.getPacketPriority()) ? -1 : (p1.getPacketPriority() > p2.getPacketPriority()) ? 1 : 0;
		}
	}

	/*
	 * public static class TimeComparator implements Comparator<FutureEvent> {
	 * 
	 * @Override public int compare(FutureEvent p1, FutureEvent p2) { return
	 * (p1.getTime() < p2.getTime()) ? -1 : (p1.getTime() > p2.getTime()) ? 1 :
	 * 0; } }
	 */
}
