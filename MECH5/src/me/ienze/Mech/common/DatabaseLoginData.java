package me.ienze.Mech.common;

public class DatabaseLoginData {

	private String database;
	private String username;
	private String password;

	public DatabaseLoginData(String database, String username, String password) {
		this.database = database;
		this.username = username;
		this.password = password;
	}

	public String getDatabase() {
		return database;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
