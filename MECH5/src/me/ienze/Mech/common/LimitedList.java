package me.ienze.Mech.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LimitedList<T> implements List<T> {

	private Object[] elements = null;

	public LimitedList(int length) {
		elements = new Object[length];
	}

	@Override
	public boolean add(T element) {
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == null) {
				elements[i] = element;
				return true;
			}
		}
		return false;
	}

	@Override
	public void add(int index, T element) {
		if (index >= elements.length) {
			throw new ArrayIndexOutOfBoundsException("Index too big. (length: " + elements.length + ")");
		}
		if (index < 0) {
			throw new ArrayIndexOutOfBoundsException("Index too low.");
		}
		elements[index] = element;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(Collection<? extends T> c) {

		Iterator<T> iterator = (Iterator<T>) c.iterator();

		while (iterator.hasNext()) {
			if (!add(iterator.next())) {
				return false;
			}
		}

		return true;
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public boolean addAll(int index, Collection<? extends T> c) {

		Iterator<T> iterator = (Iterator<T>) c.iterator();

		int i = index;

		while (iterator.hasNext()) {
			try {
				add(i, iterator.next());
			} catch (ArrayIndexOutOfBoundsException e) {
				return false;
			}
			i++;
		}

		return true;
	}

	@Override
	public void clear() {
		for (int i = 0; i < elements.length; i++) {
			elements[i] = null;
		}
	}

	@Override
	public boolean contains(Object o) {
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == o) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public boolean containsAll(Collection c) {

		Iterator<T> iterator = c.iterator();

		while (iterator.hasNext()) {
			if (!contains(iterator.next())) {
				return false;
			}
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {
		if (index >= elements.length) {
			throw new ArrayIndexOutOfBoundsException("Index too big. (length: " + elements.length + ")");
		}
		if (index < 0) {
			throw new ArrayIndexOutOfBoundsException("Index too low.");
		}

		return (T) elements[index];
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllInstancesOf(Class<?> insClass) {
		ArrayList<T> list = new ArrayList<T>();
		for (int i = 0; i < elements.length; i++) {
			if (insClass.isInstance(elements[i])) {
				list.add((T) elements[i]);
			}
		}
		return list;
	}

	@Override
	public int indexOf(Object o) {
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == o) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] != null) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<T> iterator() {
		ArrayList<Object> toIterator = new ArrayList<Object>();
		for (Object e : elements) {
			if (e != null) {
				toIterator.add(e);
			}
		}
		return (Iterator<T>) toIterator.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		for (int i = elements.length; i <= 0; i--) {
			if (elements[i] == o) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public ListIterator<T> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		for (int i = 0; i < elements.length; i++) {
			if (elements[i] == o) {
				elements[i] = null;
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T remove(int index) {
		T obj = (T) elements[index];
		elements[index] = null;
		return obj;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public boolean removeAll(Collection c) {
		Iterator<T> iterator = c.iterator();
		boolean r = true;
		while (iterator.hasNext()) {
			if (!remove(iterator.next())) {
				r = false;
			}
		}

		return r;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean retainAll(Collection c) {
		boolean r = false;

		for (int i = 0; i < elements.length; i++) {
			if (!c.contains(elements[i])) {
				elements[i] = null;
				r = true;
			}
		}

		return r;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T set(int index, T element) {
		if (index >= elements.length) {
			throw new ArrayIndexOutOfBoundsException("Index too big. (length: " + elements.length + ")");
		}
		if (index < 0) {
			throw new ArrayIndexOutOfBoundsException("Index too low.");
		}

		T before = (T) elements[index];

		elements[index] = element;

		return before;
	}

	@Override
	public int size() {
		return elements.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public LimitedList<T> subList(int fromIndex, int toIndex) {
		LimitedList<T> list = new LimitedList<T>(toIndex - fromIndex);
		int j = 0;
		for (int i = fromIndex; i < toIndex + 1; i++) {
			list.set(j, (T) elements[i]);
			j++;
		}
		return list;
	}

	@Override
	public Object[] toArray() {
		return elements;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] toArray(Object[] a) {
		return elements;
	}

	@Override
	public String toString() {
		return Arrays.toString(elements);
	}
}
