package me.ienze.Mech.common;

import me.ienze.Mech.data.value.FutureValueAreaData;

import org.apache.commons.codec.binary.Base64;

public class ByteJsonCompiler {

	public static String compile(FutureValueAreaData bytes, long time) {
		return compile(bytes.getValue(time));
	}

	public static String compile(LimitedList<Byte> bytes) {

		byte[] bytet = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++) {
			bytet[i] = (byte) (bytes.get(i) + 128);
		}

		return compile(bytet);
	}

	private static String compile(byte[] vali) {

		String base64bytes = Base64.encodeBase64String(vali);

		return base64bytes;
	}
}
