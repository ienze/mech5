package me.ienze.Mech.common;

import me.ienze.Mech.data.value.FutureValueInteger;

public interface Damagable {

	public FutureValueInteger getHealth();

	public void damage(int damage, long time);
}
