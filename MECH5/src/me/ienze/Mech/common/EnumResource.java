package me.ienze.Mech.common;

public enum EnumResource {

	COAL(), OIL(), GOLD();

	private EnumResource() {

	}

	public static int count() {
		return 3;
	}
}
