package me.ienze.Mech.common;

public interface InventoryOwner {

	void updateInventorySlot(int slot, long time);

}
