package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class LongValueLoader extends ObjectValueLoader<Long> {

	public LongValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, Long value) {
		bb.putLong(value == null ? 0 : value);
	}

	@Override
	protected Long decompileValue(ByteBuffer bb) {
		return bb.getLong();
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<Long> value) {
		return value.getEntriesCount() * 8;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<Long> value) {
		return 8;
	}
}
