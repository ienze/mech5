package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class ByteValueLoader extends ObjectValueLoader<Byte> {

	public ByteValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, Byte value) {
		bb.put(value);
	}

	@Override
	protected Byte decompileValue(ByteBuffer bb) {
		return bb.get();
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<Byte> value) {
		return value.getEntriesCount();
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<Byte> value) {
		return 1;
	}
}
