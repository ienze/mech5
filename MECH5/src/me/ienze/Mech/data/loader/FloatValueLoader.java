package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class FloatValueLoader extends ObjectValueLoader<Float> {

	public FloatValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, Float value) {
		bb.putFloat(value == null ? 0 : value);
	}

	@Override
	protected Float decompileValue(ByteBuffer bb) {
		return bb.getFloat();
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<Float> value) {
		return value.getEntriesCount() * 4;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<Float> value) {
		return 4;
	}
}
