package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class StringValueLoader extends ObjectValueLoader<String> {

	public StringValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, String value) {
		writeString(bb, value);
	}

	@Override
	protected String decompileValue(ByteBuffer bb) {
		return readString(bb);
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<String> value) {
		int totalBytesLength = 0;
		for (long time : value.getChangeTimes()) {
			totalBytesLength += getStringWriteLength(value.getValue(time));
		}
		return totalBytesLength;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<String> value) {
		return getStringWriteLength(value.getValue());
	}

	private String readString(ByteBuffer bb) {

		int stringLength = bb.getShort();

		byte[] bytes = new byte[stringLength];

		bb.get(bytes);

		return new String(bytes);
	}

	private void writeString(ByteBuffer bb, String string) {

		byte[] bytes = string.getBytes();

		bb.putShort((short) bytes.length);

		bb.put(bytes);
	}

	private int getStringWriteLength(String string) {
		return 2 + string.getBytes().length;
	}
}
