package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public abstract class ObjectValueLoader<T> {

	private Database database;

	public ObjectValueLoader(Database database) {
		this.database = database;
	}

	public T loadValueSimple(String id) {

		try {
			PreparedStatement statement = database.prepare("SELECT * FROM data WHERE id = ?");
			statement.setString(1, id);

			ResultSet result = statement.executeQuery();
			if (result.next()) {

				byte[] bytes = result.getBytes("value");

				return decompileSimpleValue(id, bytes);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void saveValueSimple(SimpleValue<T> value) {

		byte[] bytes = compileSimpleValue(value);

		saveToDatabase(bytes, value.getID());
	}

	public void saveToDatabase(byte[] bytes, String id) {

		try {
			PreparedStatement statement = database
					.prepare("INSERT INTO data (id, value) VALUES (?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)");

			statement.setString(1, id);
			statement.setBytes(2, bytes);

			// extude
			if (statement.executeUpdate() < 1) {
				System.out.println("Problem! SQL didnt updated (or inserted) value to database! (id=" + id + ", bytes="
						+ Arrays.toString(bytes) + ")");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public HashMap<Long, T> loadValueFuture(String id) {

		try {
			PreparedStatement statement = database.prepare("SELECT * FROM data WHERE id = ?");
			statement.setString(1, id);

			ResultSet result = statement.executeQuery();
			if (result.next()) {

				byte[] bytes = result.getBytes("value");

				return decompileFutureValue(id, bytes);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void saveValueFuture(FutureValue<T> value) {

		byte[] bytes = compileFutureValue(value);

		saveToDatabase(bytes, value.getID());
	}

	private byte[] compileSimpleValue(SimpleValue<T> value) {
		ByteBuffer bb = ByteBuffer.allocate(getDataSizeToAlocate(value));
		bb.rewind();

		compileValue(bb, value.getValue());

		return bb.array();
	}

	private T decompileSimpleValue(String id, byte[] bytes) {
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		bb.rewind();

		return decompileValue(bb);
	}

	private byte[] compileFutureValue(FutureValue<T> value) {

		int entriesCount = value.getEntriesCount();

		ByteBuffer bb = ByteBuffer.allocate(4 + 16 * value.getEntriesCount() + getDataSizeToAlocate(value));
		bb.rewind();

		bb.putInt(entriesCount);

		for (long time : value.getChangeTimes()) {
			bb.putLong(time);
			compileValue(bb, value.getValue(time));
		}

		return bb.array();
	}

	private HashMap<Long, T> decompileFutureValue(String id, byte[] bytes) {
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		bb.rewind();

		int entriesCount = bb.getInt();

		HashMap<Long, T> value = new HashMap<Long, T>();

		for (int i = 0; i < entriesCount; i++) {
			long time = bb.getLong();
			value.put(time, decompileValue(bb));
		}

		return value;
	}

	protected abstract void compileValue(ByteBuffer bb, T value);

	protected abstract T decompileValue(ByteBuffer bb);

	protected abstract int getDataSizeToAlocate(SimpleValue<T> value);

	protected abstract int getDataSizeToAlocate(FutureValue<T> value);
}
