package me.ienze.Mech.data.loader;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.botParts.BotParts;
import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.FutureValueInteger;
import me.ienze.Mech.data.value.FutureValueString;
import me.ienze.Mech.data.value.SimpleValue;
import me.ienze.Mech.data.value.SimpleValueInteger;
import me.ienze.Mech.data.value.SimpleValueString;
import me.ienze.Mech.inventory.Item;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.Player;

public abstract class ValueLoaderDatabase {

	private Database database;

	private ByteValueLoader byteLoader;
	private IntegerValueLoader intLoader;
	private LongValueLoader longLoader;
	private StringValueLoader stringLoader;
	private ItemsListValueLoader itemsListLoader;
	private ByteListValueLoader bytelistLoader;
	private FloatValueLoader floatLoader;

	public ValueLoaderDatabase(Database database) {

		this.database = database;

		database.open();

		if (!database.isTable("data")) {
			try {
				database.query("CREATE TABLE data (" + "id VARCHAR(255) NOT NULL, " + "value BLOB NOT NULL" + ")");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		byteLoader = new ByteValueLoader(database);
		intLoader = new IntegerValueLoader(database);
		stringLoader = new StringValueLoader(database);
		longLoader = new LongValueLoader(database);
		itemsListLoader = new ItemsListValueLoader(database);
		bytelistLoader = new ByteListValueLoader(database);
		floatLoader = new FloatValueLoader(database);
	}

	public void close() {
		database.close();
	}

	/**
	 * 
	 * Simple values
	 * 
	 */

	public Byte loadByteSimple(String id) {
		return byteLoader.loadValueSimple(id);
	}

	public Integer loadIntSimple(String id) {
		return intLoader.loadValueSimple(id);
	}

	public Long loadLongSimple(String id) {
		return longLoader.loadValueSimple(id);
	}

	public String loadStringSimple(String id) {
		return stringLoader.loadValueSimple(id);
	}

	public Map loadMapSimple(String id) {
		return Mech.getMapsHandler().getMap(stringLoader.loadValueSimple(id));
	}

	public Player loadPlayerSimple(String id) {
		return Mech.getPlayersManager().getPlayer(stringLoader.loadValueSimple(id));
	}

	public BotParts loadBotPartsSimple(String id) {
		return new BotParts(intLoader.loadValueSimple(id));
	}

	public ArrayList<Item> loadItemsListSimple(String id) {
		return itemsListLoader.loadValueSimple(id);
	}

	public LimitedList<Byte> loadByteListSimple(String id) {
		return bytelistLoader.loadValueSimple(id);
	}

	public Float loadFloatSimple(String id) {
		return floatLoader.loadValueSimple(id);
	}

	public void saveByteSimple(SimpleValue<Byte> value) {
		byteLoader.saveValueSimple(value);
	}

	public void saveIntSimple(SimpleValue<Integer> value) {
		intLoader.saveValueSimple(value);
	}

	public void saveLongSimple(SimpleValue<Long> value) {
		longLoader.saveValueSimple(value);
	}

	public void saveStringSimple(SimpleValue<String> value) {
		stringLoader.saveValueSimple(value);
	}

	public void saveMapSimple(SimpleValue<Map> value) {
		stringLoader.saveValueSimple(new SimpleValueString(value.getID(), value.getValue().getName()));
	}

	public void savePlayerSimple(SimpleValue<Player> value) {
		stringLoader.saveValueSimple(new SimpleValueString(value.getID(), value.getValue().getName()));
	}

	public void saveBotPartsSimple(SimpleValue<BotParts> value) {
		intLoader.saveValueSimple(new SimpleValueInteger(value.getID(), value.getValue().getPartsInt()));
	}

	public void saveItemsListSimple(SimpleValue<ArrayList<Item>> value) {
		itemsListLoader.saveValueSimple(value);
	}

	public void saveByteListSimple(SimpleValue<LimitedList<Byte>> value) {
		bytelistLoader.saveValueSimple(value);
	}

	public void saveFloatSimple(SimpleValue<Float> value) {
		floatLoader.saveValueSimple(value);
	}

	/**
	 * 
	 * Future values
	 * 
	 */

	public HashMap<Long, Byte> loadByteFuture(String id) {
		return byteLoader.loadValueFuture(id);
	}

	public HashMap<Long, Integer> loadIntFuture(String id) {
		return intLoader.loadValueFuture(id);
	}

	public HashMap<Long, Long> loadLongFuture(String id) {
		return longLoader.loadValueFuture(id);
	}

	public HashMap<Long, String> loadStringFuture(String id) {
		return stringLoader.loadValueFuture(id);
	}

	public HashMap<Long, Map> loadMapFuture(String id) {
		HashMap<Long, Map> maps = new HashMap<Long, Map>();

		HashMap<Long, String> names = stringLoader.loadValueFuture(id);
		if(names != null) {
			for (Long time : names.keySet()) {
				maps.put(time, Mech.getMapsHandler().getMap(names.get(time)));
			}
		}

		return maps;
	}

	public HashMap<Long, Player> loadPlayerFuture(String id) {
		HashMap<Long, Player> players = new HashMap<Long, Player>();

		HashMap<Long, String> names = stringLoader.loadValueFuture(id);
		if(names != null) {
			for (Long time : names.keySet()) {
				players.put(time, Mech.getPlayersManager().getPlayer(names.get(time)));
			}
		}

		return players;
	}

	public HashMap<Long, BotParts> loadBotPartsFuture(String id) {
		HashMap<Long, BotParts> botParts = new HashMap<Long, BotParts>();

		HashMap<Long, Integer> ints = intLoader.loadValueFuture(id);
		if(ints != null) {
			for (Long time : ints.keySet()) {
				botParts.put(time, new BotParts(ints.get(time)));
			}
		}

		return botParts;
	}

	public HashMap<Long, ArrayList<Item>> loadItemsListFuture(String id) {
		return itemsListLoader.loadValueFuture(id);
	}

	public HashMap<Long, LimitedList<Byte>> loadByteListFuture(String id) {
		return bytelistLoader.loadValueFuture(id);
	}

	public HashMap<Long, Float> loadFloatFuture(String id) {
		return floatLoader.loadValueFuture(id);
	}

	public void saveByteFuture(FutureValue<Byte> value) {
		byteLoader.saveValueFuture(value);
	}

	public void saveIntFuture(FutureValue<Integer> value) {
		intLoader.saveValueFuture(value);
	}

	public void saveLongFuture(FutureValue<Long> value) {
		longLoader.saveValueFuture(value);
	}

	public void saveStringFuture(FutureValue<String> value) {
		stringLoader.saveValueFuture(value);
	}

	public void saveMapFuture(FutureValue<Map> value) {
		FutureValueString names = new FutureValueString(value.getID());

		for (Long time : value.getChangeTimes()) {
			names.addValueChange(time, value.getValue(time).getName());
		}

		stringLoader.saveValueFuture(names);
	}

	public void savePlayerFuture(FutureValue<Player> value) {
		FutureValueString names = new FutureValueString(value.getID());

		for (Long time : value.getChangeTimes()) {
			names.addValueChange(time, value.getValue(time).getName());
		}

		stringLoader.saveValueFuture(names);
	}

	public void saveBotPartsFuture(FutureValue<BotParts> value) {
		FutureValueInteger names = new FutureValueInteger(value.getID());

		for (Long time : value.getChangeTimes()) {
			names.addValueChange(time, value.getValue(time).getPartsInt());
		}

		intLoader.saveValueFuture(names);
	}

	public void saveItemsListFuture(FutureValue<ArrayList<Item>> value) {
		itemsListLoader.saveValueFuture(value);
	}

	public void saveByteListFuture(FutureValue<LimitedList<Byte>> value) {
		bytelistLoader.saveValueFuture(value);
	}

	public void saveFloatFuture(FutureValue<Float> value) {
		floatLoader.saveValueFuture(value);
	}

	public boolean existValueId(String id) {

		try {
			PreparedStatement statement = database.prepare("SELECT id FROM data WHERE id = ?");
			statement.setString(1, id);

			ResultSet result = statement.executeQuery();
			if (result.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean removeValue(String id) {
		
		try {
			PreparedStatement statement = database.prepare("DELETE FROM data WHERE id = ?");
			statement.setString(1, id);

			if(statement.executeUpdate() > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
