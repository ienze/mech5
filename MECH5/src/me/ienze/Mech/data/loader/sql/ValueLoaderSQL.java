package me.ienze.Mech.data.loader.sql;

import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.SQLite;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;

public class ValueLoaderSQL extends ValueLoaderDatabase {

	public ValueLoaderSQL() {
		super(new SQLite(Logger.getLogger("Mech"), "[Mech]", "database", "areas"));
	}

}
