package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class ByteListValueLoader extends ObjectValueLoader<LimitedList<Byte>> {

	public ByteListValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, LimitedList<Byte> value) {

		bb.putInt(value.size());

		for (int i = 0; i < value.size(); i++) {
			bb.put(value.get(i));
		}
	}

	@Override
	protected LimitedList<Byte> decompileValue(ByteBuffer bb) {

		int bytesCount = bb.getInt();

		LimitedList<Byte> bytesList = new LimitedList<Byte>(bytesCount);

		for (int j = 0; j < bytesCount; j++) {
			bytesList.set(j, bb.get());
		}

		return bytesList;
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<LimitedList<Byte>> value) {
		int totalBytesLength = 0;
		for (long time : value.getChangeTimes()) {
			totalBytesLength += 4 + value.getValue(time).size();
		}
		return totalBytesLength;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<LimitedList<Byte>> value) {
		return 4 + value.getValue().size();
	}
}
