package me.ienze.Mech.data.loader.mysql;

import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.MySQL;
import me.ienze.Mech.common.DatabaseLoginData;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;

public class ValueLoaderMySQL extends ValueLoaderDatabase {

	public ValueLoaderMySQL(DatabaseLoginData login) {
		super(new MySQL(Logger.getLogger("Mech"), "[Mech]", login.getDatabase(), login.getUsername(), login.getPassword()));
	}
}
