package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;
import me.ienze.Mech.inventory.Item;

public class ItemsListValueLoader extends ObjectValueLoader<ArrayList<Item>> {

	public ItemsListValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, ArrayList<Item> value) {

		bb.putInt(value.size());

		for (int i = 0; i < value.size(); i++) {
			Item item = value.get(i);

			bb.put(item.getType());
			bb.put(item.getSubType());
			bb.putInt(item.getAmount());
		}
	}

	@Override
	protected ArrayList<Item> decompileValue(ByteBuffer bb) {

		int itemsCount = bb.getInt();

		ArrayList<Item> items = new ArrayList<Item>(itemsCount);

		for (int j = 0; j < itemsCount; j++) {

			byte type = bb.get();
			byte subType = bb.get();
			int amount = bb.getInt();

			items.set(j, new Item(type, subType, amount));
		}

		return items;
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<ArrayList<Item>> value) {
		int totalBytesLength = 0;
		for (long time : value.getChangeTimes()) {
			totalBytesLength += 4 + value.getValue(time).size() * 3;
		}
		return totalBytesLength;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<ArrayList<Item>> value) {
		return 4 + value.getValue().size() * 3;
	}
}
