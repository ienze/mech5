package me.ienze.Mech.data.loader;

import java.nio.ByteBuffer;

import lib.PatPeter.SQLibrary.Database;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.SimpleValue;

public class IntegerValueLoader extends ObjectValueLoader<Integer> {

	public IntegerValueLoader(Database database) {
		super(database);
	}

	@Override
	protected void compileValue(ByteBuffer bb, Integer value) {
		bb.putInt(value == null ? 0 : value);
	}

	@Override
	protected Integer decompileValue(ByteBuffer bb) {
		return bb.getInt();
	}

	@Override
	protected int getDataSizeToAlocate(FutureValue<Integer> value) {
		return value.getEntriesCount() * 4;
	}

	@Override
	protected int getDataSizeToAlocate(SimpleValue<Integer> value) {
		return 4;
	}
}
