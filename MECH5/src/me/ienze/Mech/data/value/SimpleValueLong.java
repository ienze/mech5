package me.ienze.Mech.data.value;

public class SimpleValueLong extends SimpleValue<Long> {

	public SimpleValueLong(String valueid) {
		super(valueid);
	}

	public SimpleValueLong(String valueid, Long value) {
		super(valueid, value);
	}

	@Override
	protected Long load() {
		return valueLoader.loadLongSimple(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveLongSimple(this);
	}
}
