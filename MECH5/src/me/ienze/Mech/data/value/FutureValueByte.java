package me.ienze.Mech.data.value;

import java.util.HashMap;

public class FutureValueByte extends FutureValue<Byte> {

	public FutureValueByte(String valueid) {
		super(valueid);
	}

	public FutureValueByte(String valueid, Byte value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Byte> load() {
		return valueLoader.loadByteFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveByteFuture(this);
	}
}
