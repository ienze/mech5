package me.ienze.Mech.data.value;

import java.io.Closeable;

import me.ienze.Mech.map.MapsHandler;

public class FutureTimer extends Thread implements Closeable {

	public static final long stepsDifference = 500;

	private final long updateTime = 2000;

	private boolean running = true;
	private MapsHandler mapsHandler;

	public FutureTimer(MapsHandler mapsHandler) {
		super("FutureTimer");
		this.mapsHandler = mapsHandler;
	}

	@Override
	public void run() {
		while (running) {
			try {
				Thread.sleep(updateTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			mapsHandler.update();
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public void close() {
		running = false;
		if (getState() == State.WAITING) {
			this.stop();
		}
	}
}
