package me.ienze.Mech.data.value;

import java.util.HashMap;

public class FutureValueFloat extends FutureValue<Float> {

	public FutureValueFloat(String valueid) {
		super(valueid);
	}

	public FutureValueFloat(String valueid, Float value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Float> load() {
		return valueLoader.loadFloatFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveFloatFuture(this);
	}
}
