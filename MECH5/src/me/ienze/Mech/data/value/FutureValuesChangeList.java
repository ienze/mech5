package me.ienze.Mech.data.value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FutureValuesChangeList extends HashMap<FutureValue<?>, List<Long>> {

	private static final long serialVersionUID = -7454621617083667732L;

	public void addChangeToValue(FutureValue<?> value, Long changeTime) {

		if (!containsKey(value)) {
			put(value, new ArrayList<Long>());
		}
		get(value).add(changeTime);
	}

	public void rollbackChnages() {
		for (FutureValue<?> val : keySet()) {
			for (Long time : get(val)) {
				val.removeValueChange(time);
			}
		}
	}
}
