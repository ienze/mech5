package me.ienze.Mech.data.value;

import java.util.ArrayList;

import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.map.generator.GenerationType;
import me.ienze.Mech.map.generator.IMapGenerator;
import me.ienze.Mech.map.Area;
import me.ienze.Mech.map.mapTiles.data.TileData;
import me.ienze.Mech.map.mapTiles.MapTile;

public class FutureValueAreaData extends FutureValueByteList {

	public FutureValueAreaData(String valueid, IMapGenerator mapGenerator, GenerationType genType, int genData, int x, int y) {
		super(valueid, Area.areaSize * Area.areaSize);

		if (value.isEmpty()) {
			addValueChange(0, mapGenerator.generate(genType, genData, x, y));
		}
	}

	@Override
	public LimitedList<Byte> createValue(long time) {
		LimitedList<Byte> newValue = new LimitedList<Byte>(Area.areaSize * Area.areaSize);
		this.value.put(time, newValue);
		return newValue;
	}

	public ArrayList<TileData> getAllTileData() {
		ArrayList<TileData> tileData = new ArrayList<TileData>();

		ArrayList<Long> times = new ArrayList<Long>();
		times.addAll(this.value.keySet());

		for (int t = 0; t < times.size(); t++) {
			long time = times.get(t);

			LimitedList<Byte> tiles = this.value.get(time);
			for (int i = 0; i < tiles.size(); i++) {

				int y = (int) Math.floor((float) i / (float) Area.areaSize);
				int x = i - (Area.areaSize * y);

				Byte tileId = tiles.get(i);
				MapTile tile = MapTile.getTileById(tileId);

				if (tile.hasTileData()) {

					long fromTime = time;
					long toTime = Long.MAX_VALUE;
					int j = 1;
					while (t + j < times.size() && this.value.get(times.get(t + j)).get(i) == tileId) {
						j++;
					}

					if (t + j < times.size()) {
						toTime = times.get(t + j);
					}

					TileData d = tile.createTileData(x, y, fromTime, toTime);

					if (d != null) {
						boolean alreadyContains = false;
						for (TileData td : tileData) {
							if (td.equals(d)) {
								alreadyContains = true;
							}
						}

						if (!alreadyContains) {
							tileData.add(d);
						}
					}
				}
			}
		}

		return tileData;
	}
}
