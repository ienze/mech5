package me.ienze.Mech.data.value;

public class SimpleValueInteger extends SimpleValue<Integer> {

	public SimpleValueInteger(String valueid) {
		super(valueid);
	}

	public SimpleValueInteger(String valueid, Integer value) {
		super(valueid, value);
	}

	@Override
	protected Integer load() {
		return valueLoader.loadIntSimple(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveIntSimple(this);
	}
}
