package me.ienze.Mech.data.value;

import me.ienze.Mech.Mech;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;

public abstract class SimpleValue<T> {

	protected boolean isLoaded;

	protected ValueLoaderDatabase valueLoader;
	protected String valueid;
	protected T value;

	public SimpleValue(String valueid) {
		this.valueLoader = Mech.getValueLoader();
		this.valueid = valueid;

		if (!isLoaded) {
			T newValue = load();
			if (newValue != null) {
				this.value = newValue;
			}
			isLoaded = true;
		}
	}

	public SimpleValue(String valueid, T value) {
		this(valueid);

		if (!isLoaded) {
			T newValue = load();
			if (newValue != null) {
				this.value = newValue;
			} else {
				setValue(value);
			}
			isLoaded = true;
		}
	}

	public synchronized T getValue() {

		if (!isLoaded) {
			T newValue = load();
			if (newValue != null) {
				this.value = newValue;
			}
			isLoaded = true;
		}

		return this.value;
	}

	public synchronized void setValue(T value) {
		this.value = value;

		save();
	}

	public String getID() {
		return valueid;
	}
	
	public void remove() {
		valueLoader.removeValue(valueid);
	}

	protected abstract T load();

	protected abstract void save();

	@Override
	public String toString() {
		return "FV[" + this.value.toString() + "]";
	}
}
