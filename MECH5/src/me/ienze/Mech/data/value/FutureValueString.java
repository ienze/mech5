package me.ienze.Mech.data.value;

import java.util.HashMap;

public class FutureValueString extends FutureValue<String> {

	public FutureValueString(String valueid) {
		super(valueid);
	}

	public FutureValueString(String valueid, String value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, String> load() {
		return valueLoader.loadStringFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveStringFuture(this);
	}
}
