package me.ienze.Mech.data.value;

import java.util.HashMap;

public class FutureValueInteger extends FutureValue<Integer> {

	public FutureValueInteger(String valueid) {
		super(valueid);
	}

	public FutureValueInteger(String valueid, Integer value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Integer> load() {
		return valueLoader.loadIntFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveIntFuture(this);
	}
}
