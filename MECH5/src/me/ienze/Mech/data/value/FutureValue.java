package me.ienze.Mech.data.value;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import me.ienze.Mech.Mech;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;

public abstract class FutureValue<T> {

	protected boolean isLoaded = false;

	protected ValueLoaderDatabase valueLoader;
	protected String valueid;
	protected HashMap<Long, T> value = new HashMap<Long, T>();

	public FutureValue(String valueid) {
		this.valueLoader = Mech.getValueLoader();
		this.valueid = valueid;

		if (!isLoaded) {
			HashMap<Long, T> loadedValue = load();
			if (loadedValue != null) {
				this.value = loadedValue;
			}
			isLoaded = true;
		}
	}

	public FutureValue(String valueid, T value) {
		this(valueid);
		addValueChange(0l, value);
	}

	public T getValue(Long time) {

		if (!isLoaded) {
			HashMap<Long, T> loadedValue = load();
			if (loadedValue != null) {
				this.value = loadedValue;
			}
			isLoaded = true;
		}

		if (value.containsKey(time)) {
			return value.get(time);
		}

		long bt = 0;

		Iterator<Long> iterator = value.keySet().iterator();

		while (iterator.hasNext()) {
			Long t = iterator.next();

			if (t <= time) {
				if (t > bt) {
					bt = t;
				}
			}
		}

		return value.get(bt);
	}

	public void addValueChange(long time, T value) {
		this.value.put(time, value);

		save();
	}

	public int getEntriesCount() {
		return this.value.size();
	}

	public Set<Long> getChangeTimes() {
		return this.value.keySet();
	}

	public void removeValueChange(Long time) {
		value.remove(time);
	}

	public String getID() {
		return valueid;
	}
	
	public void remove() {
		valueLoader.removeValue(valueid);
	}

	protected abstract HashMap<Long, T> load();

	protected abstract void save();

	@Override
	public String toString() {
		return "FV[" + this.value.toString() + "]";
	}
}
