package me.ienze.Mech.data.value;

public class SimpleValueString extends SimpleValue<String> {

	public SimpleValueString(String valueid) {
		super(valueid);
	}

	public SimpleValueString(String valueid, String value) {
		super(valueid, value);
	}

	@Override
	protected String load() {
		return valueLoader.loadStringSimple(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveStringSimple(this);
	}
}
