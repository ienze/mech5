package me.ienze.Mech.data.value;

import me.ienze.Mech.map.Map;

public class SimpleValueMap extends SimpleValue<Map> {

	public SimpleValueMap(String valueid) {
		super(valueid);
	}

	public SimpleValueMap(String valueid, Map value) {
		super(valueid, value);
	}

	@Override
	protected Map load() {
		return valueLoader.loadMapSimple(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveMapSimple(this);
	}
}
