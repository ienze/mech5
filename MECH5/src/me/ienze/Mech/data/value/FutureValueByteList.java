package me.ienze.Mech.data.value;

import java.util.HashMap;

import me.ienze.Mech.common.LimitedList;

public class FutureValueByteList extends FutureValue<LimitedList<Byte>> {

	private int listSize;

	public FutureValueByteList(String valueid, int listSize) {
		super(valueid);
		this.listSize = listSize;
	}

	public FutureValueByteList(String valueid, LimitedList<Byte> value) {
		super(valueid, value);
		this.listSize = value.size();
	}

	@Override
	protected HashMap<Long, LimitedList<Byte>> load() {
		return valueLoader.loadByteListFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveByteListFuture(this);
	}

	public LimitedList<Byte> createValue(long time) {
		LimitedList<Byte> newValue = new LimitedList<Byte>(listSize);
		this.value.put(time, newValue);
		return newValue;
	}
}
