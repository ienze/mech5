package me.ienze.Mech.data.value;

import java.util.ArrayList;
import java.util.HashMap;

import me.ienze.Mech.inventory.Item;

public class FutureValueItemsList extends FutureValue<ArrayList<Item>> {

	public FutureValueItemsList(String valueid) {
		super(valueid);
	}

	public FutureValueItemsList(String valueid, ArrayList<Item> value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, ArrayList<Item>> load() {
		return valueLoader.loadItemsListFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveItemsListFuture(this);
	}

	public ArrayList<Item> createValue(long time) {
		if (this.value.containsKey(time)) {
			return this.value.get(time);
		} else {
			ArrayList<Item> newValue = new ArrayList<Item>();
			this.value.put(time, newValue);
			return newValue;
		}
	}
}
