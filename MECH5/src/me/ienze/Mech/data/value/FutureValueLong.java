package me.ienze.Mech.data.value;

import java.util.HashMap;

public class FutureValueLong extends FutureValue<Long> {

	public FutureValueLong(String valueid) {
		super(valueid);
	}

	public FutureValueLong(String valueid, Long value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Long> load() {
		return valueLoader.loadLongFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveLongFuture(this);
	}
}
