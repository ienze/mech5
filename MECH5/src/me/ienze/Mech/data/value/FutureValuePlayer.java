package me.ienze.Mech.data.value;

import java.util.HashMap;

import me.ienze.Mech.player.Player;

public class FutureValuePlayer extends FutureValue<Player> {

	public FutureValuePlayer(String valueid) {
		super(valueid);
	}

	public FutureValuePlayer(String valueid, Player value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Player> load() {
		return valueLoader.loadPlayerFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.savePlayerFuture(this);
	}
}
