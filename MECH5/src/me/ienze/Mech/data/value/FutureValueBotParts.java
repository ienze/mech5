package me.ienze.Mech.data.value;

import java.util.HashMap;

import me.ienze.Mech.bot.botParts.BotParts;

public class FutureValueBotParts extends FutureValue<BotParts> {

	public FutureValueBotParts(String valueid) {
		super(valueid);
	}

	public FutureValueBotParts(String valueid, BotParts value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, BotParts> load() {
		return valueLoader.loadBotPartsFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveBotPartsFuture(this);
	}
}
