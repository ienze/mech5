package me.ienze.Mech.data.value;

import java.util.HashMap;

import me.ienze.Mech.map.Map;

public class FutureValueMap extends FutureValue<Map> {

	public FutureValueMap(String valueid) {
		super(valueid);
	}

	public FutureValueMap(String valueid, Map value) {
		super(valueid, value);
	}

	@Override
	protected HashMap<Long, Map> load() {
		return valueLoader.loadMapFuture(valueid);
	}

	@Override
	protected void save() {
		valueLoader.saveMapFuture(this);
	}
}
