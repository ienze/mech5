package me.ienze.Mech.commands;

import java.util.List;

public abstract class CommandShowList implements Command {

	private static final int entriesPerPage = 10;

	protected void showList(List<?> list, int page) {
		System.out.println("page " + page + ":");

		for (int i = page * entriesPerPage; i < page * entriesPerPage + entriesPerPage; i++) {
			if (i < list.size()) {
				System.out.println(list.get(i));
			}
		}
	}
}
