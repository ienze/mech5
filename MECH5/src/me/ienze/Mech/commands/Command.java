package me.ienze.Mech.commands;

public interface Command {

	public void runCommand(String[] args);

	public String getDescription();
}
