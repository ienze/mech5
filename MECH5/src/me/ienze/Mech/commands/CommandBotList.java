package me.ienze.Mech.commands;

import me.ienze.Mech.Mech;

public class CommandBotList extends CommandShowList {

	@Override
	public void runCommand(String[] args) {

		int page = 0;

		if (args.length > 0) {
			try {
				page = Integer.valueOf(args[0]);
			} catch (Exception e) {
			}
		}

		showList(Mech.getBotManager().getBots(), page);
	}

	@Override
	public String getDescription() {
		return "show list of all bots, args: <page>";
	}

}
