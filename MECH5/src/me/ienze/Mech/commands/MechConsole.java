package me.ienze.Mech.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class MechConsole {

	private boolean enabled = true;
	
	private HashMap<String, Command> commandsMap = new HashMap<String, Command>();

	public MechConsole() {

		// register commands
		registerCommand("help", new CommandHelp());
		registerCommand("stop", new CommandStop());
		registerCommand("newBot", new CommandNewBot());
		registerCommand("botList", new CommandBotList());
		registerCommand("removeBots", new CommandRemoveBots());
		registerCommand("mapList", new CommandMapList());

	}

	public void start() {

		// run console reading loop
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		while (enabled) {
			try {

				String line = br.readLine();
				if(line != null) {
					String cmd = line.trim();
	
					if (cmd != null && !cmd.isEmpty()) {
	
						String[] cmdargs = cmd.split(" ");
						String[] args = new String[0];
						if (cmdargs.length > 1) {
							args = Arrays.copyOfRange(cmdargs, 1, cmdargs.length);
						}
						runCommand(cmdargs[0], args);
	
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void stop() {
		this.enabled = false;
	}

	private void runCommand(String command, String[] args) {
		if (commandsMap.containsKey(command)) {
			commandsMap.get(command).runCommand(args);
		} else {
			System.out.println("Incorrect command. Do \"help\" for list fo commands.");
		}
	}

	public void registerCommand(String label, Command command) {
		if (!commandsMap.containsKey(label)) {
			commandsMap.put(label, command);
		} else {
			System.out.println("Duplicate command registration for command " + label + " (old: " + commandsMap.get(label) + ", new: "
					+ command + ")");
		}
	}

	public HashMap<String, Command> getCommandsMap() {
		return commandsMap;
	}
}
