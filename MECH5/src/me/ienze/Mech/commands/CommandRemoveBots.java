package me.ienze.Mech.commands;

import me.ienze.Mech.Mech;

public class CommandRemoveBots implements Command {

	@Override
	public void runCommand(String[] args) {

		Mech.getBotManager().removeAllBots();
		
		System.out.println("all bots removed.");
	}

	@Override
	public String getDescription() {
		return "remove all saved bots";
	}

}
