package me.ienze.Mech.commands;

import me.ienze.Mech.Mech;

public class CommandStop implements Command {

	@Override
	public void runCommand(String[] args) {
		Mech.stop();
	}

	@Override
	public String getDescription() {
		return "stop server";
	}

}
