package me.ienze.Mech.commands;

import java.util.HashMap;

import me.ienze.Mech.Mech;

public class CommandHelp implements Command {

	@Override
	public void runCommand(String[] args) {
		HashMap<String, Command> commands = Mech.getConsole().getCommandsMap();

		for (String command : commands.keySet()) {
			System.out.println(command + " - " + commands.get(command).getDescription());
		}
	}

	@Override
	public String getDescription() {
		return "prints list of commands";
	}

}
