package me.ienze.Mech.commands;

import java.util.Arrays;

import me.ienze.Mech.Mech;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.Player;

public class CommandNewBot implements Command {

	@Override
	public void runCommand(String[] args) {

		if (args.length >= 4) {
			Map map = null;
			int x = 0, y = 0;
			Player player = null;

			try {
				map = Mech.getMapsHandler().getMap(args[0]);
				x = Integer.valueOf(args[1]);
				y = Integer.valueOf(args[2]);
				player = Mech.getPlayersManager().getPlayer(args[3]);
			} catch (Exception e) {
			}

			if (map != null && player != null) {
				Mech.getBotManager().createNewBot(map, x, y, player);
				System.out.println("new bot added.");
			} else {
				System.out.println("some argument is incorrect");
			}

		} else {
			System.out.println("not enought arguments given");
		}
	}

	@Override
	public String getDescription() {
		return "spawn new bot, args: <map> <x> <y> <owner>";
	}

}
