package me.ienze.Mech.commands;

import me.ienze.Mech.Mech;

public class CommandMapList extends CommandShowList {

	@Override
	public void runCommand(String[] args) {

		int page = 0;

		if (args.length > 0) {
			try {
				page = Integer.valueOf(args[0]);
			} catch (Exception e) {
			}
		}

		showList(Mech.getMapsHandler().getMaps(), page);
	}

	@Override
	public String getDescription() {
		return "show list of all maps, args: <page>";
	}

}
