package me.ienze.Mech.player.playerWindow;

import org.json.simple.JSONObject;

public class PlayerWindowData {

	private String windowType;
	private JSONObject content;
	
	public PlayerWindowData(String windowType, JSONObject content) {
		super();
		this.windowType = windowType;
		this.content = content;
	}
	
	public String getWindowType() {
		return windowType;
	}
	
	public JSONObject getContent() {
		return content;
	}
}
