package me.ienze.Mech.player.playerWindow;

import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public interface PlayerWindow {

	public JSONObject getContent(Player player, String[] request);
	
	public String getWindowType();
}
