package me.ienze.Mech.player.playerWindow;

import java.util.HashMap;

import me.ienze.Mech.player.Player;

public class PlayerWindowHandler {

	private HashMap<String, PlayerWindow> windows = new HashMap<String, PlayerWindow>();
	
	public PlayerWindowHandler() {
		windows.put("quests", new PlayerWindowQuests());
	}
	
	public PlayerWindowData getWindowData(Player player, String requestString) {
		String[] request = requestString.split("/.");
		
		PlayerWindow windowProvider = windows.get(request[0]);
		if(windowProvider != null) {
			return new PlayerWindowData(windowProvider.getWindowType(), windowProvider.getContent(player, request));
		}
		return null;
	}
}
