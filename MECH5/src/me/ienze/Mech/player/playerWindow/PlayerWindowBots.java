package me.ienze.Mech.player.playerWindow;

import java.util.ArrayList;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class PlayerWindowBots implements PlayerWindow {

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getContent(Player player, String[] request) {
		JSONObject json = new JSONObject();
		
		ArrayList<Bot> bots = player.getOwnedBots(player.getShowTime());
		
		JSONArray botsArray = new JSONArray();
		
		for(Bot bot : bots) {
			JSONObject questJson = new JSONObject();
			
			questJson.put("id", bot.getId());
			
			botsArray.add(questJson);
		}
		
		json.put("bots", botsArray);
		
		return json;
	}

	@Override
	public String getWindowType() {
		return "BOTS";
	}
}
