package me.ienze.Mech.player.playerWindow;

import java.util.ArrayList;

import me.ienze.Mech.player.Player;
import me.ienze.Mech.storyline.PlayerQuest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class PlayerWindowQuests implements PlayerWindow {

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getContent(Player player, String[] request) {
		JSONObject json = new JSONObject();
		
		ArrayList<PlayerQuest> quests = player.getQuestHandler().getAllQuests();
		
		JSONArray questsArray = new JSONArray();
		
		for(PlayerQuest quest : quests) {
			JSONObject questJson = new JSONObject();
			
			questJson.put("id", quest.getId());
			questJson.put("timeFrom", quest.getTimeFrom());
			questJson.put("timeTo", quest.getTimeTo());
			questJson.put("finished", quest.isFinished());
			
			questsArray.add(questJson);
		}
		
		json.put("quests", questsArray);
		
		return json;
	}

	@Override
	public String getWindowType() {
		return "QUESTS";
	}
}
