package me.ienze.Mech.player;

import java.util.ArrayList;

import me.ienze.Mech.Mech;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;
import me.ienze.Mech.events.PlayerLoginEvent;
import me.ienze.Mech.events.PlayerLogoutEvent;

public class PlayersManager {

	private ArrayList<Player> onlinePlayers = new ArrayList<Player>();

	public PlayersManager(ValueLoaderDatabase valueLoader) {
		
	}

	public void onLogin(Player player) {
		onlinePlayers.add(player);

		Mech.getEvents().callEvent(new PlayerLoginEvent(player));
	}

	public void onLogout(Player player) {
		if(onlinePlayers.remove(player)) {
			Mech.getEvents().callEvent(new PlayerLogoutEvent(player));
		}
	}

	public ArrayList<Player> getOnlinePlayers() {
		return onlinePlayers;
	}

	public Player getPlayer(String name) {
		for (Player player : onlinePlayers) {
			if (player.getName().equals(name)) {
				return player;
			}
		}
		
		return new Player(name);
	}

	public boolean checkPassword(String name, String password) {

		if (!getPlayer(name).isRegistred()) {
			return false;
		}

		return getPlayer(name).checkPassword(password);
	}

	public boolean register(String name, String password) {
		Player player = getPlayer(name);
		if (player.isRegistred()) {
			return false;
		} else {
			player.onRegister(password);
			return true;
		}
	}
}
