package me.ienze.Mech.player;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.common.Comparators;
import me.ienze.Mech.common.FutureValueUtils;
import me.ienze.Mech.common.InventoryOwner;
import me.ienze.Mech.common.PasswordHash;
import me.ienze.Mech.connection.Packet;
import me.ienze.Mech.connection.Packet004MoveCam;
import me.ienze.Mech.connection.Packet006Inventory;
import me.ienze.Mech.connection.Packet007SlotAction;
import me.ienze.Mech.connection.Packet044AreaUpdate;
import me.ienze.Mech.connection.Packet046BotUpdate;
import me.ienze.Mech.connection.Packet047BotLeave;
import me.ienze.Mech.data.value.SimpleValueInteger;
import me.ienze.Mech.data.value.SimpleValueMap;
import me.ienze.Mech.data.value.SimpleValueString;
import me.ienze.Mech.inventory.Inventory;
import me.ienze.Mech.map.Area;
import me.ienze.Mech.map.AreaCoordinates;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.playerWindow.PlayerWindowData;
import me.ienze.Mech.storyline.UserQuestHandler;
import me.ienze.SimpleEvents.Event;
import me.ienze.SimpleEvents.EventHandler;

import org.json.simple.JSONObject;

public class Player implements InventoryOwner {

	public static final int max_name_length = 24;
	public static final int max_password_length = 64;

	private String name;
	private ArrayList<Packet> packetsToSend = new ArrayList<Packet>();
	private String session;

	private SimpleValueInteger mapLoadingX;
	private SimpleValueInteger mapLoadingY;

	private ArrayList<AreaCoordinates> loadedAreas = new ArrayList<AreaCoordinates>();

	private Inventory inventory;

	private long showTime;

	private long lastPacketUpdate;

	private SimpleValueString password;
	private SimpleValueMap openedMap;

	private UserQuestHandler questHandler;

	public Player(String name) {
		this.name = name;

		mapLoadingX = new SimpleValueInteger("player." + name + ".camX", 0);
		mapLoadingY = new SimpleValueInteger("player." + name + ".camY", 0);

		openedMap = new SimpleValueMap("player." + name + ".world");
		password = new SimpleValueString("player." + name + ".pass");

		inventory = new Inventory("player-" + name, 9, this);

		this.questHandler = new UserQuestHandler(this, Mech.getQuestLoader());
	}

	public String getName() {
		return name;
	}

	public void onPacketUpdate() {
		lastPacketUpdate = System.currentTimeMillis();
	}

	public long getLastPacketUpdate() {
		return lastPacketUpdate;
	}

	public synchronized void addPacket(Packet packet) {
		packetsToSend.add(packet);
	}

	public synchronized ArrayList<Packet> getPackets() {
		return packetsToSend;
	}

	@SuppressWarnings("unchecked")
	public synchronized ArrayList<Packet> getPacketsSorted() {
		ArrayList<Packet> list = (ArrayList<Packet>) getPackets().clone();
		Collections.sort(list, new Comparators.PacketComparator());
		return list;
	}

	public synchronized void setPackets(ArrayList<Packet> packetsToSend) {
		this.packetsToSend = packetsToSend;
	}

	public void setMapLoadingCenter(int x, int y) {
		this.mapLoadingX.setValue(x >> Area.areaSizeBits);
		this.mapLoadingY.setValue(y >> Area.areaSizeBits);
		checkMapLoad();
	}

	public AreaCoordinates getMapLoadingCenter() {
		return new AreaCoordinates(mapLoadingX.getValue(), mapLoadingY.getValue());
	}

	public void checkMapLoad() {
		if (openedMap != null) {
			for (int x = mapLoadingX.getValue() - Area.fastLoadDistance; x < mapLoadingX.getValue() + Area.fastLoadDistance; x++) {
				for (int y = mapLoadingY.getValue() - Area.fastLoadDistance; y < mapLoadingY.getValue() + Area.fastLoadDistance; y++) {
					AreaCoordinates a = new AreaCoordinates(x, y);

					if (!loadedAreas.contains(a)) {
						loadArea(a);
					}
				}
			}
		}
	}
	
	public void onChangeShowTime(long timeFrom, long timeTo) {
		
		//update areas
		for (AreaCoordinates coords : loadedAreas) {
			Area area = getMap().getArea(coords);
			if (area.haveChanged(timeFrom, timeTo)) {
				updateArea(area);
			}
		}

		//update bots
		ArrayList<Bot> oldBots = getMap().getBots(timeFrom);
		ArrayList<Bot> newBots = getMap().getBots(timeTo);

		for (Bot bot : oldBots) {
			//update cuttent bots
			if (newBots.contains(bot)) {
				if (bot.haveChanged(timeFrom, timeTo)) {
					updateBot(bot, timeTo);
				}
				
				newBots.remove(bot);
			//remove bots
			} else {
				updateBotLeave(bot);
			}
		}
		
		//add new bots
		for (Bot bot : newBots) {
			updateBot(bot, timeTo);
		}
		
		//update inventory
		if(FutureValueUtils.doesChange(inventory.getItems(), timeFrom, timeTo)) {
			updateInventory(timeTo);
		}
	}

	private void updateArea(Area area) {
		addPacket(new Packet044AreaUpdate(this, area));
	}
	
	private void updateBot(Bot bot, long time) {
		addPacket(new Packet046BotUpdate(bot, time));
	}
	
	private void updateBotLeave(Bot bot) {
		addPacket(new Packet047BotLeave(bot));
	}
	
	private void updateInventory(long time) {
		addPacket(new Packet006Inventory(inventory, time));
	}

	public void loadArea(AreaCoordinates area) {
		addPacket(new Packet044AreaUpdate(this, openedMap.getValue().getArea(area)));
		loadedAreas.add(area);
	}

	public boolean haveAreaLoaded(AreaCoordinates area) {
		return loadedAreas.contains(area);
	}

	public void setOpenedMap(Map map) {
		this.openedMap.setValue(map);
		loadedAreas.clear();
	}

	public Map getMap() {
		return openedMap.getValue();
	}

	public void onLogin() {
		int x = 0;
		int y = 0;

		this.loadedAreas.clear();

		this.showTime = Mech.getActualTime();
		
		Mech.getPlayersManager().onLogin(this);

		if (openedMap.getValue() == null) {
			openedMap.setValue(Mech.getMapsHandler().getMainWorld());
		}

		setMapLoadingCenter(x, y);

		addPacket(new Packet004MoveCam(x, y));

		for (Bot bot : Mech.getBotManager().getBots()) {
			if (bot.getMap(showTime) == openedMap.getValue()) {
				addPacket(new Packet046BotUpdate(bot, showTime));
			}
		}

		addPacket(new Packet006Inventory(inventory, showTime));
	}

	public void onRegister(String password) {

		try {
			this.password.setValue(PasswordHash.createHash(password));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		// TODO
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player) {
			return ((Player) obj).getName().equals(this.getName());
		}
		return false;
	}

	@Override
	public void updateInventorySlot(int slot, long time) {

		if (time < this.showTime) {
			return;
		}

		addPacket(new Packet007SlotAction(inventory.getId(), slot, getInventory().getItem(slot, time)));
	}

	public boolean checkPassword(String pass) {

		try {
			return PasswordHash.validatePassword(pass, password.getValue());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean isRegistred() {
		return password.getValue() != null;
	}

	public long getShowTime() {
		return showTime;
	}

	public void setShowTime(Long time) {
		if(showTime != time) {
			onChangeShowTime(showTime, time);
			showTime = time;
		}
	}

	@EventHandler
	public void onEvent(Event event) {
		questHandler.callQuestsEvent(event);
		// TODO maybe centralize this a bit more.. or add chcek if events player
		// is same
	}

	public UserQuestHandler getQuestHandler() {
		return questHandler;
	}
	
	public void setSession(String session) {
		this.session = session;
	}

	public String getSession() {
		return this.session;
	}

	public PlayerWindowData getWindowData(String specialRequest) {
		return Mech.getPlayerWindowHandler().getWindowData(this, specialRequest);
	}

	public ArrayList<Bot> getOwnedBots(long time) {
		return Mech.getBotManager().getBotsOwnedBy(this, time);
	}

	@Override
	public String toString() {
		return "Player[name=" + name + ", map=" + openedMap.getValue().getName() + "]";
	}
}
