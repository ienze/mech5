package me.ienze.Mech.map.generator;

import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.map.Area;
import me.ienze.noise.SimplexOctaveGenerator;

public class MapGeneratorFlat implements IMapGenerator {

	private SimplexOctaveGenerator randomGen1;

	private int areaLength = Area.areaSize * Area.areaSize;

	public MapGeneratorFlat(long seed) {

		randomGen1 = new SimplexOctaveGenerator(seed, 6);
		randomGen1.setScale(1 / 64.0);
	}

	@Override
	public LimitedList<Byte> generate(GenerationType genType, int genData, int x, int y) {

		LimitedList<Byte> data = new LimitedList<Byte>(areaLength);

		for (int nx = 0; nx < Area.areaSize; nx++) {
			for (int ny = 0; ny < Area.areaSize; ny++) {
				if (genType == GenerationType.TILES) {
					data.set(Area.areaSize * nx + ny, (byte) (Math.random() > 0.94 ? 1 : 0));

				} else if (genType == GenerationType.HEIGHT) {
					data.set(Area.areaSize * nx + ny, (byte) 0);

				} else if (genType == GenerationType.RESOURCES) {
					data.set(
							Area.areaSize * nx + ny,
							(byte) (randomGen1.noise((x * Area.areaSize) + nx, (y * Area.areaSize) + ny, 0.2 * genData, 0.56 / genData) * 40.0 * genData));
				}
			}
		}

		return data;
	}
}
