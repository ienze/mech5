package me.ienze.Mech.map.generator;

import me.ienze.Mech.common.LimitedList;

public interface IMapGenerator {

	public LimitedList<Byte> generate(GenerationType genType, int genData, int x, int y);
}
