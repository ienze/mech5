package me.ienze.Mech.map;

import java.util.ArrayList;

import me.ienze.Mech.common.EnumResource;
import me.ienze.Mech.common.FutureValueUtils;
import me.ienze.Mech.data.value.FutureValueAreaData;
import me.ienze.Mech.map.generator.GenerationType;
import me.ienze.Mech.map.mapTiles.data.TileData;
import me.ienze.Mech.map.mapTiles.MapTile;

public class Area {

	public static final int areaSize = 64;
	public static int areaSizeBits = 6;
	public static int fastLoadDistance = 2;

	private Map map;

	private int x, y;

	private FutureValueAreaData tiles;
	private FutureValueAreaData height;

	private ArrayList<FutureValueAreaData> resources = new ArrayList<FutureValueAreaData>();

	private ArrayList<TileData> tileData = null;

	public Area(Map map, int x, int y) {
		this.map = map;
		this.x = x;
		this.y = y;

		this.tiles = new FutureValueAreaData("map." + map.getName() + ".area." + x + "." + y + ".tiles", map.getMapGenerator(),
				GenerationType.TILES, 0, x, y);

		tileData = this.tiles.getAllTileData();

		this.height = new FutureValueAreaData("map." + map.getName() + ".area." + x + "." + y + ".height", map.getMapGenerator(),
				GenerationType.HEIGHT, 0, x, y);

		for (int i = 0; i < EnumResource.count(); i++) {
			this.resources.add(i,
					new FutureValueAreaData("map." + map.getName() + ".area." + x + "." + y + ".resources." + i, map.getMapGenerator(),
							GenerationType.RESOURCES, i, x, y));
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Map getMap() {
		return map;
	}

	public MapTile getTile(int localX, int localY, long time) {
		return MapTile.getTileById(tiles.getValue(time).get(areaSize * localY + localX));
	}

	public void setTile(int localX, int localY, MapTile tile, long time) {
		tiles.createValue(time).set(areaSize * localY + localX, tile.getTileId());
	}

	public void setTile(int localX, int localY, Byte tile, long time) {
		tiles.createValue(time).set(areaSize * localY + localX, tile);
	}

	public int getHeight(int localX, int localY, long time) {
		return height.getValue(time).get(areaSize * localY + localX);
	}

	public void setHeight(int localX, int localY, int newHeight, long time) {
		height.createValue(time).set(areaSize * localY + localX, (byte) newHeight);
	}

	public int getResourceLevel(int localX, int localY, int resourceType, long time) {
		return resources.get(resourceType).getValue(time).get(areaSize * localY + localX);
	}

	public void setResourceLevel(int localX, int localY, int resourceType, int resourceLevel, long time) {
		resources.get(resourceType).createValue(time).set(areaSize * localY + localX, (byte) resourceLevel);
	}

	public ArrayList<TileData> getTileDataList() {
		return tileData;
	}

	public TileData getTileData(int localX, int localY, long time) {
		for (TileData td : tileData) {
			if (td.getX() == localX && td.getY() == localY && td.isInTime(time)) {
				return td;
			}
		}
		return null;
	}

	public FutureValueAreaData getDataTiles() {
		return tiles;
	}

	public FutureValueAreaData getDataHeight() {
		return height;
	}

	public FutureValueAreaData getDataResources(int i) {
		return resources.get(i);
	}

	public AreaCoordinates getAreaCoordinates() {
		return new AreaCoordinates(x, y);
	}
	
	public boolean haveChanged(long timeFrom, long timeTo) {
		if(FutureValueUtils.doesChange(this.tiles, timeFrom, timeTo)) {
			return true;
		}
		
		if(FutureValueUtils.doesChange(this.height, timeFrom, timeTo)) {
			return true;
		}
		
		/*
		 * resources dont have to update becosue players dont see them
		for(int i=0; i<EnumResource.count(); i++) {
			FutureValueUtils.doesChange(this.resources.get(i), timeFrom, timeTo);
		}
		*/
		return false;
	}
}
