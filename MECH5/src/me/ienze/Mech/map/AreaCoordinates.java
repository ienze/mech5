package me.ienze.Mech.map;

import java.nio.ByteBuffer;

public class AreaCoordinates {

	private int areaX;
	private int areaY;

	public AreaCoordinates(int areaX, int areaY) {
		this.areaX = areaX;
		this.areaY = areaY;
	}

	public int getAreaX() {
		return areaX;
	}

	public int getAreaY() {
		return areaY;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AreaCoordinates) {
			return (((AreaCoordinates) obj).areaX == areaX && ((AreaCoordinates) obj).areaY == areaY);
		}
		return false;
	}

	public double distance(AreaCoordinates areaCoordinates) {
		int dx = Math.abs(areaCoordinates.getAreaX() - getAreaX());
		int dy = Math.abs(areaCoordinates.getAreaY() - getAreaY());
		return Math.sqrt(dx * dx + dy + dy);
	}

	@Override
	public int hashCode() {
		byte[] pos = ByteBuffer.allocate(8).putInt(areaX).putInt(areaY).array();

		ByteBuffer result = ByteBuffer.allocate(4);
		result.put(pos[2]);
		result.put(pos[3]);
		result.put(pos[6]);
		result.put(pos[7]);
		result.rewind();
		int hash = result.getInt();

		return hash;
	}

	public static AreaCoordinates forPos(int localX, int localY) {
		return new AreaCoordinates(localX >> Area.areaSizeBits, localY >> Area.areaSizeBits);
	}
}
