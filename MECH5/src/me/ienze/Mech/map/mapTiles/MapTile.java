package me.ienze.Mech.map.mapTiles;

import me.ienze.Mech.map.mapTiles.data.TileData;

public abstract class MapTile {

	static private MapTile[] tiles = new MapTile[127];

	static private MapTile defaultTile;

	static {
		defaultTile = new TileDefault(0);
		new TileWater(8);
		new TileTown(100);
	}

	public MapTile(int id) {
		tiles[id] = this;
	}

	private byte tileId;

	public static MapTile getTileById(byte id) {
		MapTile t = tiles[id];
		if (t != null) {
			return t;
		} else {
			return defaultTile;
		}
	}

	public boolean canMove() {
		return true;
	}

	public boolean hasTileData() {
		return false;
	}

	public TileData createTileData(int x, int y, long fromTime, long toTime) {
		return null;
	}

	public Byte getTileId() {
		return tileId;
	}
}
