package me.ienze.Mech.map.mapTiles.data;

public abstract class TileData {

	private int x;
	private int y;
	private long timeFrom;
	private long timeTo;

	public TileData(int x, int y, long timeFrom, long timeTo) {
		this.x = x;
		this.y = y;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public long getTimeFrom() {
		return timeFrom;
	}

	public long getTimeTo() {
		return timeTo;
	}

	public boolean isInTime(long time) {
		return time >= timeFrom && time < timeTo;
	}
}
