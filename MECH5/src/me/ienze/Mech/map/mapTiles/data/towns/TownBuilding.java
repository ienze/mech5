package me.ienze.Mech.map.mapTiles.data.towns;

import me.ienze.Mech.data.value.FutureValueInteger;

import org.json.simple.JSONObject;

public abstract class TownBuilding {

	private FutureValueInteger level;
	private int x;
	private int y;

	public TownBuilding(int x, int y) {
		this.level = new FutureValueInteger("tiledata.town." + x + "." + y + ".building." + getBuildingType() + ".level", 0);
		this.x = x;
		this.y = y;
	}

	public int getLevel(long time) {
		return level.getValue(time);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	protected abstract int getBuildingType();

	@SuppressWarnings("unchecked")
	public JSONObject getJsonData(boolean complex, long time) {
		JSONObject json = new JSONObject();

		json.put("type", getBuildingType());
		json.put("level", this.level.getValue(time));

		return json;
	}
}
