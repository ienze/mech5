package me.ienze.Mech.map.mapTiles.data.towns;

import me.ienze.Mech.common.InventoryOwner;
import me.ienze.Mech.inventory.Inventory;

public class BuildingStock extends TownBuilding implements InventoryOwner {

	private Inventory inventory;

	public BuildingStock(int x, int y) {
		super(x, y);
		inventory = new Inventory("stock-" + x + "-" + y, this);
	}

	@Override
	public void updateInventorySlot(int slot, long time) {
		// TODO: send packet to players with opened inventories
	}

	public Inventory getInventory() {
		return inventory;
	}

	@Override
	protected int getBuildingType() {
		return TownBuildingType.STOCK.getId();
	}
}
