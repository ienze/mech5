package me.ienze.Mech.map.mapTiles.data.towns;

public class BuildingWorkshop extends TownBuilding {

	public BuildingWorkshop(int x, int y) {
		super(x, y);
	}

	@Override
	protected int getBuildingType() {
		return TownBuildingType.WORKSHOP.getId();
	}
}
