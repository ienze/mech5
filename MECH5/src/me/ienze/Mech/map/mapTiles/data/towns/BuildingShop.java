package me.ienze.Mech.map.mapTiles.data.towns;

import java.util.HashMap;

import me.ienze.Mech.data.value.FutureValueFloat;

public class BuildingShop extends TownBuilding {

	private HashMap<String, FutureValueFloat> pricesCache = new HashMap<String, FutureValueFloat>();

	public BuildingShop(int x, int y) {
		super(x, y);
	}

	public void setPrice(byte type, byte subtype, float price, long time) {
		String key = type + ":" + subtype;

		if (pricesCache.containsKey(key)) {
			pricesCache.get(key).addValueChange(time, price);
		} else {
			FutureValueFloat priceValue = new FutureValueFloat("tiledata.town." + getX() + "." + getY() + ".building." + getBuildingType()
					+ ".prices." + type + "." + subtype, 0f);
			priceValue.addValueChange(time, price);
			pricesCache.put(key, priceValue);
		}

	}

	public float getPrice(byte type, byte subtype, long time) {
		String key = type + ":" + subtype;

		if (pricesCache.containsKey(key)) {
			return pricesCache.get(key).getValue(time);
		} else {
			FutureValueFloat priceValue = new FutureValueFloat("tiledata.town." + getX() + "." + getY() + ".building." + getBuildingType()
					+ ".prices." + type + "." + subtype, 0f);
			pricesCache.put(key, priceValue);
			return priceValue.getValue(time);
		}
	}

	@Override
	protected int getBuildingType() {
		return TownBuildingType.SHOP.getId();
	}
}
