package me.ienze.Mech.map.mapTiles.data;

import me.ienze.Mech.player.playerWindow.PlayerWindowData;

import org.json.simple.JSONObject;

public interface TileDataWindow {

	public PlayerWindowData getTileWindowData(String specialRequest, long time);
}
