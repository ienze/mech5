package me.ienze.Mech.map.mapTiles.data;

import me.ienze.Mech.common.Damagable;
import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.data.value.FutureValueInteger;
import me.ienze.Mech.map.mapTiles.data.towns.TownBuilding;
import me.ienze.Mech.map.mapTiles.data.towns.TownBuildingType;
import me.ienze.Mech.player.playerWindow.PlayerWindowData;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TileDataTown extends TileData implements TileDataWindow, Damagable {

	// TODO private TownPermissions permissions;
	private LimitedList<TownBuilding> buildings = new LimitedList<TownBuilding>(TownBuildingType.getCount());
	private FutureValueInteger health;

	public TileDataTown(int x, int y, long timeFrom, long timeTo) {
		super(x, y, timeFrom, timeTo);
		// permissions = new TownPermissions(new JSONObject());
		
		health = new FutureValueInteger("tiledata.town." + x + "." + y + ".health", 0);

		for (int i = 0; i < buildings.size(); i++) {
			buildings.set(i, TownBuildingType.getById(i).newInstance(x, y));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public PlayerWindowData getTileWindowData(String specialRequest, long time) {
		JSONObject json = null;

		if (specialRequest != null && !specialRequest.isEmpty()) {

			if (specialRequest.startsWith("buildingInfo.")) {

				String buildingInfo = specialRequest.substring(specialRequest.indexOf('.') + 1, specialRequest.length());

				try {
					int bi = Integer.valueOf(buildingInfo);
					TownBuilding build = this.buildings.get(bi);

					if (build != null) {
						json = build.getJsonData(true, time);
					}

				} catch (Exception e) {
				}
			}

		} else {
			json = new JSONObject();
			json.put("buildings", getBuildingsData(time));
			json.put("health", health.getValue(time));
		}

		return new PlayerWindowData("TILE_TOWN", json);
	}

	@SuppressWarnings("unchecked")
	private JSONArray getBuildingsData(long time) {
		JSONArray json = new JSONArray();

		for (int i = 0; i < this.buildings.size(); i++) {
			json.add(this.buildings.get(i).getJsonData(false, time));
		}

		return json;
	}

	@Override
	public FutureValueInteger getHealth() {
		return health;
	}

	@Override
	public void damage(int damage, long time) {
		health.addValueChange(time, health.getValue(time) - damage);
	}
}
