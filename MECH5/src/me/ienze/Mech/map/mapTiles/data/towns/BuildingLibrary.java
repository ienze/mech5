package me.ienze.Mech.map.mapTiles.data.towns;

import org.json.simple.JSONObject;

public class BuildingLibrary extends TownBuilding {

	public BuildingLibrary(int x, int y) {
		super(x, y);
	}

	@Override
	protected int getBuildingType() {
		return TownBuildingType.LIBRARY.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getJsonData(boolean complex, long time) {
		JSONObject json = super.getJsonData(complex, time);

		if (complex) {
			json.put("books", null); // TODO
		}
		return json;
	}
}
