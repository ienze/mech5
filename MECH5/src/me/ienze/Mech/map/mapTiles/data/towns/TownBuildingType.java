package me.ienze.Mech.map.mapTiles.data.towns;

import java.lang.reflect.Constructor;

public enum TownBuildingType {

	SHOP(0, BuildingShop.class), LIBRARY(1, BuildingLibrary.class), WORKSHOP(2, BuildingWorkshop.class), STOCK(3, BuildingStock.class);

	private int id;
	private Class<? extends TownBuilding> buildingClass;

	private TownBuildingType(int id, Class<? extends TownBuilding> buildingClass) {
		this.id = id;
		this.buildingClass = buildingClass;
	}

	public static TownBuildingType getByClass(Class<? extends TownBuilding> buildingClass) {
		for (TownBuildingType t : TownBuildingType.values()) {
			if (t.buildingClass == buildingClass) {
				return t;
			}
		}
		return null;
	}

	public static TownBuildingType getById(int buildingId) {
		for (TownBuildingType t : TownBuildingType.values()) {
			if (t.id == buildingId) {
				return t;
			}
		}
		return null;
	}

	public int getId() {
		return id;
	}

	public Class<? extends TownBuilding> getBuildingClass() {
		return buildingClass;
	}

	private static int count;
	static {
		count = TownBuildingType.values().length;
	}

	public static int getCount() {
		return count;
	}

	public TownBuilding newInstance(int x, int y) {

		try {
			Constructor<? extends TownBuilding> constructor = this.buildingClass.getConstructor(int.class, int.class);
			return constructor.newInstance(x, y);
		} catch (Exception e) {
			System.out.println("Problem loading town building class: "+this.buildingClass.getName());
			e.printStackTrace();
		}

		return null;
	}
}
