package me.ienze.Mech.map.mapTiles;

import me.ienze.Mech.map.mapTiles.data.TileData;
import me.ienze.Mech.map.mapTiles.data.TileDataTown;

public class TileTown extends MapTile {

	public TileTown(int id) {
		super(id);
	}

	@Override
	public TileData createTileData(int x, int y, long fromTime, long toTime) {
		return new TileDataTown(x, y, fromTime, toTime);
	}

	@Override
	public boolean hasTileData() {
		return true;
	}
}
