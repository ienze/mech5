package me.ienze.Mech.map.mapTiles;

public class TileWater extends MapTile {

	public TileWater(int id) {
		super(id);
	}

	@Override
	public boolean canMove() {
		return false;
	}
}
