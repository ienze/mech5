package me.ienze.Mech.map;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import me.ienze.Mech.data.loader.ValueLoaderDatabase;
import me.ienze.Mech.map.generator.MapGenerator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MapsHandler {

	private File worldsFile = new File("resources/worlds.json");
	private JSONArray worlds = new JSONArray();
	private ArrayList<Map> maps = new ArrayList<Map>(1);

	public MapsHandler(ValueLoaderDatabase valuelLoader) {

		try {
			if (!worldsFile.exists()) {
				worldsFile.getParentFile().mkdirs();
				worldsFile.createNewFile();

				worlds = new JSONArray();

				close();
			} else {
				worlds = (JSONArray) new JSONParser().parse(new FileReader(worldsFile));
			}
			JSONToMaps();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public Map getMap(String name) {
		for (Map map : maps) {
			if (map.getName().equals(name)) {
				return map;
			}
		}
		return null;
	}

	public Map getMap(int id) {
		return maps.get(id);
	}

	public Map createMap(String name, long seed) {
		Map map = new Map(name, seed, new MapGenerator(seed));

		maps.add(map);

		return map;
	}

	public void close() {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter(worldsFile));

			mapsToJSON();

			output.write(worlds.toJSONString());

			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void mapsToJSON() {
		JSONArray json = new JSONArray();

		int i = 0;
		for (Map map : maps) {
			JSONObject mapJson = new JSONObject();

			mapJson.put("id", i);
			mapJson.put("name", map.getName());
			mapJson.put("seed", map.getSeed());

			json.add(mapJson);

			i++;
		}

		this.worlds = json;
	}

	private void JSONToMaps() {
		for (Object mapJsonObject : this.worlds) {
			JSONObject mapJson = (JSONObject) mapJsonObject;

			int id = ((Long) mapJson.get("id")).intValue();

			long seed = (Long) mapJson.get("seed");

			maps.add(id, new Map((String) mapJson.get("name"), seed, new MapGenerator(seed)));
		}
	}

	public ArrayList<Map> getMaps() {
		return maps;
	}

	public void update() {
		// TODO Auto-generated method stub

	}

	public Map getMainWorld() {
		return getMap(0);
	}
}
