package me.ienze.Mech.map;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.common.Damagable;
import me.ienze.Mech.common.EnumResource;
import me.ienze.Mech.common.FutureValueUtils;
import me.ienze.Mech.map.generator.IMapGenerator;
import me.ienze.Mech.map.mapTiles.data.TileData;
import me.ienze.Mech.map.mapTiles.MapTile;

public class Map {

	public static final int max_name_length = 24;
	public static final float miningConstant = 0.46f;

	private long seed = 123;
	private String name = "World";
	private IMapGenerator mapGenerator;

	private HashMap<AreaCoordinates, Area> areasCache = new HashMap<AreaCoordinates, Area>();

	public Map(String name, long seed) {
		this.name = name;
		this.seed = seed;
	}

	public Map(String name, long seed, IMapGenerator mapGenerator) {
		this(name, seed);
		this.mapGenerator = mapGenerator;
	}

	public Area getArea(int x, int y) {
		AreaCoordinates areaCoord = new AreaCoordinates(x, y);

		Area area = areasCache.get(areaCoord);
		if (area == null) {
			area = new Area(this, x, y);

			areasCache.put(areaCoord, area);
		}
		return area;
	}

	public Area getArea(AreaCoordinates area) {
		return getArea(area.getAreaX(), area.getAreaY());
	}

	public MapTile getTile(int x, int y, long time) {
		int ax = x >> Area.areaSizeBits;
		int ay = y >> Area.areaSizeBits;
		Area area = getArea(ax, ay);

		return area.getTile(x - (ax * Area.areaSizeBits), y - (ay * Area.areaSizeBits), time);
	}

	public TileData getTileData(int x, int y, long time) {
		int ax = x >> Area.areaSizeBits;
		int ay = y >> Area.areaSizeBits;
		Area area = getArea(ax, ay);

		return area.getTileData(x - (ax * Area.areaSizeBits), y - (ay * Area.areaSizeBits), time);
	}

	public int[] mine(int x, int y, long time) {
		int ax = x >> Area.areaSizeBits;
		int ay = y >> Area.areaSizeBits;
		Area area = getArea(ax, ay);

		int[] mined = new int[EnumResource.count()];

		for (int i = 0; i < EnumResource.count(); i++) {
			int resource = area.getResourceLevel(x - (ax * Area.areaSizeBits), y - (ay * Area.areaSizeBits), i, time);

			int minedResource = Math.round(resource * miningConstant);

			mined[i] = minedResource;

			area.setResourceLevel(x - (ax * Area.areaSizeBits), y - (ay * Area.areaSizeBits), i, (resource - minedResource), time);
		}

		return mined;
	}

	public String getName() {
		return name;
	}

	public long getSeed() {
		return seed;
	}

	public IMapGenerator getMapGenerator() {
		return mapGenerator;
	}

	public void setMapGenerator(IMapGenerator mapGenerator) {
		this.mapGenerator = mapGenerator;
	}

	public Damagable getDamagableTarget(int x, int y, long time) {
		
		//chcek bots
		List<Bot> bots = Mech.getBotManager().getBots();
		for(Bot bot : bots) {
			if(bot.getMap(time) == this) {
				Point loc = bot.getLoc(time);
				if(loc.x == x && loc.y == y) {
					return bot;
				}
			}
		}
		
		TileData tile = getTileData(x, y, time);
		if(tile != null) {
			if(tile instanceof Damagable) {
				return ((Damagable)tile);
			}
		}
		
		return null;
	}

	public ArrayList<Bot> getBots(long time) {
		return Mech.getBotManager().getBotsOnMap(this, time);
	}
}
