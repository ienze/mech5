package me.ienze.Mech.events;

import me.ienze.Mech.bot.Bot;

public class BotMoveEvent extends BotEvent {

	private int x, y;
	
	public BotMoveEvent(Bot bot, int x, int y, long time) {
		super(bot, time);
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
