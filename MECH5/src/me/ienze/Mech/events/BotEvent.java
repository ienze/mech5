package me.ienze.Mech.events;

import me.ienze.Mech.bot.Bot;

public class BotEvent extends TimedEvent {

	private Bot bot;

	public BotEvent(Bot bot, long time) {
		super(time);
		this.bot = bot;
	}

	public Bot getBot() {
		return bot;
	}
}
