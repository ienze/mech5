package me.ienze.Mech.events;

import me.ienze.SimpleEvents.Event;

public class TimedEvent implements Event {

	private long time;

	public TimedEvent(long time) {
		this.time = time;
	}

	public long getTime() {
		return time;
	}
	
	//TODO switch some events to this superclass
}
