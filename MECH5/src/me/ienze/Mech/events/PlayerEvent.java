package me.ienze.Mech.events;

import me.ienze.Mech.player.Player;
import me.ienze.SimpleEvents.Event;

public abstract class PlayerEvent implements Event {

	private Player player;

	public PlayerEvent(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}
}
