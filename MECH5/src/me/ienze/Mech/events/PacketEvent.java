package me.ienze.Mech.events;

import me.ienze.SimpleEvents.Event;

public class PacketEvent implements Event {

	private int sendedPackets;
	private int recivedPackets;

	public PacketEvent(int sendedPackets, int recivedPackets) {
		this.sendedPackets = sendedPackets;
		this.recivedPackets = recivedPackets;
	}

	public int getRecivedPackets() {
		return recivedPackets;
	}

	public int getSendedPackets() {
		return sendedPackets;
	}
}
