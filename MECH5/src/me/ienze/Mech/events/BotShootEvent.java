package me.ienze.Mech.events;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.common.Damagable;

public class BotShootEvent extends BotEvent {

	private Damagable target;
	
	public BotShootEvent(Bot bot, Damagable target, long time) {
		super(bot, time);
		this.target = target;
	}
	
	public Damagable getTarget() {
		return target;
	}
}
