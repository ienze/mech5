package me.ienze.Mech;

import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import me.ienze.Mech.bot.BotManager;
import me.ienze.Mech.bot.botParts.PartsLoader;
import me.ienze.Mech.commands.MechConsole;
import me.ienze.Mech.common.DatabaseLoginData;
import me.ienze.Mech.connection.MechServer;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;
import me.ienze.Mech.data.loader.mysql.ValueLoaderMySQL;
import me.ienze.Mech.data.value.FutureTimer;
import me.ienze.Mech.map.MapsHandler;
import me.ienze.Mech.player.PlayersManager;
import me.ienze.Mech.player.playerWindow.PlayerWindowHandler;
import me.ienze.Mech.storyline.QuestLoader;
import me.ienze.SimpleEvents.EventFactory;

public class Mech {

	private static MechServer mechServer;
	private static FutureTimer futureTimer;
	
	private static MapsHandler mapsHandler;
	
	private static BotManager botManager;
	
	private static PlayersManager playersManager;
	private static PlayerWindowHandler playerWindowHandler;
	
	private static MechConsole console;

	private static EventFactory eventFactory;

	private static ValueLoaderDatabase valueLoader;
	private static QuestLoader questLoader;
	private static PartsLoader partsLoader;

	public static void main(String[] args) {
		init();
		startConsole();
	}

	public static void init() {

		Logger logger = Logger.getLogger("Mech");

		SimpleFormatter fmt = new SimpleFormatter();
		StreamHandler sh = new StreamHandler(System.out, fmt);
		logger.addHandler(sh);

		mechServer = new MechServer(5333);
		valueLoader = new ValueLoaderMySQL(getDatabaseLogin());
		futureTimer = new FutureTimer(mapsHandler);

		botManager = new BotManager(valueLoader);

		mapsHandler = new MapsHandler(valueLoader);
		
		playersManager = new PlayersManager(valueLoader);
		playerWindowHandler = new PlayerWindowHandler();
		
		eventFactory = new EventFactory();

		questLoader = new QuestLoader();
		partsLoader = new PartsLoader();

		botManager.init();
	}

	private static void startConsole() {
		console = new MechConsole();
		console.start();
	}

	public static MechServer getMechServer() {
		return mechServer;
	}

	public static FutureTimer getFutureTimer() {
		return futureTimer;
	}

	public static EventFactory getEvents() {
		return eventFactory;
	}

	public static long getActualTime() {
		return System.currentTimeMillis() / FutureTimer.stepsDifference;
	}

	public static PlayersManager getPlayersManager() {
		return playersManager;
	}

	public static MapsHandler getMapsHandler() {
		return mapsHandler;
	}

	public static BotManager getBotManager() {
		return botManager;
	}

	public static DatabaseLoginData getDatabaseLogin() {
		return new DatabaseLoginData("mech", "root", "4645rzrt");
	}

	public static ValueLoaderDatabase getValueLoader() {
		return valueLoader;
	}

	public static QuestLoader getQuestLoader() {
		return questLoader;
	}
	
	public static PartsLoader getPartsLoader() {
		return partsLoader;
	}

	public static MechConsole getConsole() {
		return console;
	}
	
	public static PlayerWindowHandler getPlayerWindowHandler() {
		return playerWindowHandler;
	}

	static {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				Mech.stop();
			}
		}));
	}

	public static void stop() {
		if (mechServer != null && mechServer.isAlive()) {
			mechServer.close();

			mapsHandler.close();
			futureTimer.close();

			valueLoader.close();
			
			console.stop();
		}
	}
}