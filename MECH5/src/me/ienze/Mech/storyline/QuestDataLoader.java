package me.ienze.Mech.storyline;

public interface QuestDataLoader {

	public String getQuestSavePrefix();
	
	public long getTimeFrom();

	public long getTimeTo();

	public void setTimeFrom(Long time);

	public void setTimeTo(Long time);
}
