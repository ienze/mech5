package me.ienze.Mech.storyline;

public abstract class Quest {

	private String id;

	public Quest(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public abstract boolean isFineshed(QuestDataLoader questDataLoader);

}
