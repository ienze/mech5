package me.ienze.Mech.storyline;

import me.ienze.Mech.data.value.SimpleValueLong;
import me.ienze.Mech.player.Player;

public class PlayerQuest implements QuestDataLoader {

	private Player player;
	private Quest quest;
	private SimpleValueLong fromTime;
	private SimpleValueLong toTime;

	public PlayerQuest(Player player, Quest quest) {
		this.player = player;
		this.quest = quest;

		this.fromTime = new SimpleValueLong(getQuestSavePrefix() + "timeFrom", -1L);
		this.toTime = new SimpleValueLong(getQuestSavePrefix() + "timeTo", -1L);
	}

	@Override
	public String getQuestSavePrefix() {
		return "quest." + quest.getId() + ".player." + player.getName() + ".";
	}

	public long getTimeFrom() {
		return fromTime.getValue();
	}

	public long getTimeTo() {
		return toTime.getValue();
	}

	public void setTimeFrom(Long time) {
		fromTime.setValue(time);
	}

	public void setTimeTo(Long time) {
		toTime.setValue(time);
	}

	public String getId() {
		return quest.getId();
	}

	public boolean isFinished() {
		return quest.isFineshed(this);
	}
}
