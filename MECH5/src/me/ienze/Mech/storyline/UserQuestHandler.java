package me.ienze.Mech.storyline;

import java.util.ArrayList;
import java.util.Iterator;

import me.ienze.Mech.events.TimedEvent;
import me.ienze.Mech.player.Player;
import me.ienze.SimpleEvents.Event;

public class UserQuestHandler extends QuestEventFactory {

	private Player player;
	private QuestLoader questLoader;
	private ArrayList<PlayerQuest> activeQuests = new ArrayList<PlayerQuest>();

	public UserQuestHandler(Player player, QuestLoader questLoader) {
		this.player = player;
		this.questLoader = questLoader;
	}

	public void openQuest(String id, long time) {

		PlayerQuest questExist = getQuest(id);
		if (questExist != null) {
			// quest is already opened
			return;
		}

		Quest loadedQuest = questLoader.getQuest(id);

		PlayerQuest quest = new PlayerQuest(player, loadedQuest);

		quest.setTimeFrom(time);

		addQuest(quest);
	}

	public void closeQuest(String id, long time) {

		PlayerQuest quest = getQuest(id);

		if (quest == null || quest.getTimeFrom() > time) {
			return;
		}

		quest.setTimeTo(time);
	}

	private void addQuest(PlayerQuest quest) {
		registerEvents(quest);
		activeQuests.add(quest);
	}

	public PlayerQuest getQuest(String id) {
		for (PlayerQuest quest : activeQuests) {
			if (id.equals(quest.getId())) {
				return quest;
			}
		}
		return null;
	}

	public void callQuestsEvent(Event event) {
		Iterator<PlayerQuest> quests = listeners.keySet().iterator();

		while (quests.hasNext()) {
			PlayerQuest quest = quests.next();

			if (event instanceof TimedEvent) {
				long eventTime = ((TimedEvent) event).getTime();
				if (eventTime < quest.getTimeFrom() || eventTime > quest.getTimeTo()) {
					continue;
				}
			}

			callEvent(quest, event);
		}
	}

	public ArrayList<PlayerQuest> getAllQuests() {
		return activeQuests;
	}
}
