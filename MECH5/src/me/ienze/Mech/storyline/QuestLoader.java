package me.ienze.Mech.storyline;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import me.ienze.Mech.common.PluginClassLoader;

public class QuestLoader extends PluginClassLoader<Quest> {

	private HashMap<String, Quest> quests = new HashMap<String, Quest>();
	
	public QuestLoader() {
		
		ArrayList<Quest> loadedQuests = loadPluginClasses(new File("resources/quests"));
		for (Quest loadedQuest : loadedQuests) {
			if(!this.quests.containsKey(loadedQuest.getId())) {
				this.quests.put(loadedQuest.getId(), loadedQuest);
			} else {
				System.out.println("Duplicate quest. Quest with id "+loadedQuest.getId()+" is already registred!");
			}
		}
		
		System.out.println(this.quests.size()+" quests loaded.");
	}
	


	public Quest getQuest(String questId) {
		return this.quests.get(questId);
	}
}
