package me.ienze.Mech.storyline;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import me.ienze.SimpleEvents.Event;
import me.ienze.SimpleEvents.EventHandler;

public abstract class QuestEventFactory {

	protected HashMap<PlayerQuest, ArrayList<Method>> listeners = new HashMap<PlayerQuest, ArrayList<Method>>();

	public void registerEvents(PlayerQuest object) {

		ArrayList<Method> methods = new ArrayList<Method>();

		for (Method method : object.getClass().getMethods()) {
			if (isEventMethod(method)) {
				methods.add(method);
			}
		}

		if (!methods.isEmpty()) {
			listeners.put(object, methods);
		}
	}

	private boolean isEventMethod(Method method) {
		EventHandler methodAnnotation = method.getAnnotation(EventHandler.class);
		if (methodAnnotation != null) {
			if (method.getParameterTypes().length == 2) {
				return true;
			}
		}
		return false;
	}

	public void callEvent(PlayerQuest quest, Event event) {
		for (Method method : listeners.get(quest)) {
			if (method.getParameterTypes()[0].isInstance(event)) {
				try {
					method.invoke(quest, event, quest);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
