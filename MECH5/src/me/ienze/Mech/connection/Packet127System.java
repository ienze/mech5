package me.ienze.Mech.connection;

import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet127System extends Packet {

	private enum SystemConnectionAction {
		OFFLINE, RELOAD, BAN
	}

	private SystemConnectionAction action;

	public Packet127System(Player player) {
		super(player);
	}

	public Packet127System(Player player, SystemConnectionAction action) {
		super(player);
		this.action = action;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("action", action.name().toLowerCase());

		return json;
	}
}
