package me.ienze.Mech.connection;

import me.ienze.Mech.Mech;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet001Login extends Packet {

	private boolean success;
	private String token;

	public Packet001Login(Player player) {
		super(player);
	}

	@Override
	public void onRecive(JSONObject data) {
		
		if(data.containsKey("register") && data.containsKey("name") && data.containsKey("pass")) {
			
			boolean register = Boolean.valueOf((String) data.get("register"));
			boolean joinWorld = Boolean.valueOf((String) data.get("joinWorld"));
			
			String name = (String) data.get("name");
			String password = (String) data.get("pass");
	
			if (register) {
				if (Mech.getPlayersManager().register(name, password)) {
					System.out.println(name + " has registred.");
				} else {
					System.out.println("There was problem with " + name + "'s registration.");
					success = false;
				}
	
			} else {
				success = Mech.getPlayersManager().checkPassword(name, password);
	
				if (success) {
					
					String session = (String) data.get("session");
					
					this.player = Mech.getPlayersManager().getPlayer(name);
					Mech.getMechServer().createSession(session, player);
					player.setSession(session);
					
					this.token = session;
					
					this.player.addPacket(this);
					
					if(joinWorld) {
						this.player.onLogin();
						System.out.println(name + " has logined in.");
					}
				}
			}
		}
	}
	
	public boolean isSuccess() {
		return success;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("success", success);
		if (success) {
			json.put("player", player.getName());
			json.put("token", token);
			json.put("map", Mech.getMapsHandler().getMainWorld().getName());
		}

		return json;
	}
	
	@Override
	public Packet sendHardcodedPacketOnRecive() {
		return this;
	}
}
