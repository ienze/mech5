package me.ienze.Mech.connection;

import me.ienze.Mech.common.ByteJsonCompiler;
import me.ienze.Mech.map.Area;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet044AreaUpdate extends Packet {

	private Area area;

	public Packet044AreaUpdate(Player player, Area area) {
		super(player);

		this.area = area;

		priority = (int) Math.round(Integer.MAX_VALUE - player.getMapLoadingCenter().distance(area.getAreaCoordinates()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		long time = player.getShowTime();

		json.put("x", area.getX());
		json.put("y", area.getY());

		json.put("tiles", ByteJsonCompiler.compile(area.getDataTiles(), time));
		json.put("height", ByteJsonCompiler.compile(area.getDataHeight(), time));

		return json;
	}
}
