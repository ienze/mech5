package me.ienze.Mech.connection;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet046BotUpdate extends Packet {

	private int botId;
	private String owner;
	private int x, y;

	public Packet046BotUpdate(Player player) {
		super(player);
	}

	public Packet046BotUpdate(Bot bot, long time) {
		super(null);
		this.botId = bot.getId();
		this.owner = bot.getOwner(time).getName();
		this.x = bot.getX(time);
		this.y = bot.getY(time);
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("id", botId);
		json.put("owner", owner);

		json.put("x", x);
		json.put("y", y);

		return json;
	}
}
