package me.ienze.Mech.connection;

import me.ienze.Mech.player.Player;
import me.ienze.Mech.player.playerWindow.PlayerWindowData;

import org.json.simple.JSONObject;

public class Packet009PlayerWindow extends Packet {

	private JSONObject sendData;
	private String windowType;
	private String request;

	public Packet009PlayerWindow(Player player) {
		super(player);
	}

	public Packet009PlayerWindow(JSONObject sendData, String windowType, String specialRequest) {
		super(null);
		this.sendData = sendData;
		this.windowType = windowType;
		this.request = specialRequest;
	}

	@Override
	public void onRecive(JSONObject data) {
		if (data.containsKey("request")) {
			String specialRequest = (String) data.get("request");

			PlayerWindowData playerWindowData = player.getWindowData(specialRequest);
			
			if (playerWindowData != null) {
				player.addPacket(new Packet009PlayerWindow(playerWindowData.getContent(), playerWindowData.getWindowType(), specialRequest));
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("content", sendData);
		json.put("window", windowType);
		json.put("request", request);
		
		return json;
	}
}
