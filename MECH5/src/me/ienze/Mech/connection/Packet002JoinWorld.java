package me.ienze.Mech.connection;

import me.ienze.Mech.Mech;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet002JoinWorld extends Packet {

	public Packet002JoinWorld(Player player) {
		super(player);
	}

	@Override
	public void onRecive(JSONObject data) {
		String world = (String) data.get("world");
		
		Map map = Mech.getMapsHandler().getMap(world);
		
		if(map != null) {
			this.player.setOpenedMap(map);
			this.player.onLogin();
			
			//TODO maybe edit this
			this.player.setMapLoadingCenter(0, 0);
		}
		
		player.addPacket(this);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();
		
		//update time on change world
		json.put("serverTime", Mech.getActualTime());
		
		return json;
	}
}
