package me.ienze.Mech.connection;

import org.json.simple.JSONObject;

import me.ienze.Mech.player.Player;

public class Packet000Echo extends Packet {

	public Packet000Echo(Player player) {
		super(player);
	}
	
	@Override
	public void onRecive(JSONObject data) {

		if(data.containsKey("t")) {
			try {
				long time = ((Long)data.get("t"));
				player.setShowTime(time);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}
