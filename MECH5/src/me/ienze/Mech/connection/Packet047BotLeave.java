package me.ienze.Mech.connection;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet047BotLeave extends Packet {

	private int botId;
	
	public Packet047BotLeave(Player player) {
		super(player);
	}

	public Packet047BotLeave(Bot bot) {
		super(null);
		this.botId = bot.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("id", botId);
		
		return json;
	}
}
