package me.ienze.Mech.connection;

import java.util.HashMap;

import me.ienze.Mech.Mech;
import me.ienze.Mech.map.Area;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet {

	public enum PacketList {

		ECHO(0, Packet000Echo.class),
		LOGIN(1, Packet001Login.class),
		JOIN_WORLD(2, Packet002JoinWorld.class),
		CONTROL_BOT(3,Packet003BotControl.class),
		MOVE_CAM(4, Packet004MoveCam.class),

		INVENTORY(6, Packet006Inventory.class),
		SLOT_ACTION(7, Packet007SlotAction.class),
		TILE_DATA_WINDOW(8, Packet008TileDataWindow.class),
		PLAYER_WINDOW(9, Packet009PlayerWindow.class),

		UPDATE_AREA(44, Packet044AreaUpdate.class),
		UPDATE_TILE(45, Packet045TileUpdate.class),
		BOT_UPDATE(46, Packet046BotUpdate.class),
		BOT_LEAVE(47, Packet047BotLeave.class),

		SYSTEM(127, Packet127System.class);

		private static PacketList[] idPacketMap = new PacketList[128];
		private static HashMap<Class<? extends Packet>, Byte> classIdMap = new HashMap<Class<? extends Packet>, Byte>();

		private byte id;
		private Class<? extends Packet> packetClass;

		private PacketList(int id, Class<? extends Packet> packetClass) {
			this.id = (byte) id;
			this.packetClass = packetClass;
		}

		public byte getId() {
			return id;
		}

		public Packet newPacket(Player player) {
			try {
				return (Packet) packetClass.getConstructors()[0].newInstance(player);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		public static PacketList byId(int packet) {
			return idPacketMap[packet];
		}

		public static byte getIdByClass(Class<? extends Packet> packetClass) {
			return classIdMap.get(packetClass);
		}

		static {
			for (PacketList l : PacketList.values()) {
				idPacketMap[l.id] = l;
				classIdMap.put(l.packetClass, l.id);
			}
		}
	}

	protected int priority = Integer.MAX_VALUE;
	protected Player player;

	public Packet(Player player) {
		this.player = player;
	}

	public void onRecive(JSONObject data) {

	}

	@SuppressWarnings("unchecked")
	public JSONObject toSend() {
		JSONObject json = new JSONObject();

		try {
			json.put("packet", getPacketId());
		} catch (Exception e) {
			System.out.println("There was a problem compiling packet: " + this);
		}

		return json;
	}

	public byte getPacketId() {
		return PacketList.getIdByClass(this.getClass());
	}

	public int getPacketPriority() {
		return priority;
	}

	public static void sendPacketToPlayersInArea(Packet packet, Area area) {
		for (Player player : Mech.getPlayersManager().getOnlinePlayers()) {

			if (area.getMap() == player.getMap()) {

				if (player.haveAreaLoaded(area.getAreaCoordinates())) {

					player.addPacket(packet);
				}
			}
		}
	}

	public Packet sendHardcodedPacketOnRecive() {
		return null;
	}
}
