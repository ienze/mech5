package me.ienze.Mech.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Iterator;

import me.ienze.Mech.Mech;
import me.ienze.Mech.events.PacketEvent;
import me.ienze.Mech.player.Player;

public class MechServer extends Thread {

	private ServerSocket serverSocket;

	private ConnectionTimeOutWatcher timeOutWatcher;

	private boolean running = true;
	private HashMap<String, Player> sessions = new HashMap<String, Player>();

	private int recivedPackets, sendedPackets;

	public MechServer(int port) {
		super("MechServer");

		timeOutWatcher = new ConnectionTimeOutWatcher(this);

		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Could not listen on port: " + port);
			System.exit(-1);
		}

		System.out.println("MECH server started.");

		start();
	}

	@Override
	public void run() {
		while (running) {
			try {
				new MechServerConnection(this, serverSocket.accept()).start();
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
	}

	public Player getSession(String session) {
		return sessions.get(session);
	}

	public HashMap<String, Player> getSessions() {
		return sessions;
	}

	public boolean createSession(String session, Player player) {

		Iterator<Player> sessionsIter = sessions.values().iterator();

		while (sessionsIter.hasNext()) {
			Player p = sessionsIter.next();
			if (p.equals(player)) {
				return false;
			}
		}

		sessions.put(session, player);

		return true;
	}

	public void closeSession(String session) {

		Player p = sessions.get(session);

		if (p != null) {
			Mech.getPlayersManager().onLogout(p);

			sessions.remove(session);
		}
	}

	public void closeSession(Player player) {
		if (player != null) {

			Iterator<String> sessionsIter = sessions.keySet().iterator();

			while (sessionsIter.hasNext()) {
				String sessionId = sessionsIter.next();
				if (sessions.get(sessionId).equals(player)) {
					sessions.remove(sessionId);
				}
			}

			Mech.getPlayersManager().onLogout(player);
		}
	}

	public int getRecivedPacketsCount() {
		return recivedPackets;
	}

	public int getSendedPacketsCount() {
		return sendedPackets;
	}

	public void onRecivePacket() {
		recivedPackets++;
		Mech.getEvents().callEvent(new PacketEvent(sendedPackets, recivedPackets));
	}

	public void onSendPacket() {
		sendedPackets++;
		Mech.getEvents().callEvent(new PacketEvent(sendedPackets, recivedPackets));
	}

	@SuppressWarnings("deprecation")
	public void close() {
		try {
			running = false;
			serverSocket.close();
		} catch (Exception e) {

		}

		timeOutWatcher.stop();

		System.out.println("MECH server stoped.");
	}
}
