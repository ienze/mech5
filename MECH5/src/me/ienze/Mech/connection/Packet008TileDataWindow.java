package me.ienze.Mech.connection;

import me.ienze.Mech.map.mapTiles.data.TileData;
import me.ienze.Mech.map.mapTiles.data.TileDataWindow;
import me.ienze.Mech.player.Player;
import me.ienze.Mech.player.playerWindow.PlayerWindowData;

import org.json.simple.JSONObject;

public class Packet008TileDataWindow extends Packet {

	private JSONObject sendData;
	private String windowType;
	private int tile_x, tile_y;
	private String request;

	public Packet008TileDataWindow(Player player) {
		super(player);
	}

	public Packet008TileDataWindow(JSONObject sendData, String windowType, int x, int y, String specialRequest) {
		super(null);
		this.sendData = sendData;
		this.windowType = windowType;
		this.tile_x = x;
		this.tile_y = y;
		this.request = specialRequest;
	}

	@Override
	public void onRecive(JSONObject data) {
		if (data.containsKey("tile_x") && data.containsKey("tile_y")) {
			int x = ((Long) data.get("tile_x")).intValue();
			int y = ((Long) data.get("tile_y")).intValue();
			String specialRequest = (String) data.get("request");

			TileData tileData = player.getMap().getTileData(x, y, player.getShowTime());
			if (tileData instanceof TileDataWindow) {

				PlayerWindowData playerWindowData = ((TileDataWindow) tileData).getTileWindowData(specialRequest, player.getShowTime());

				if (playerWindowData != null) {
					player.addPacket(new Packet008TileDataWindow(playerWindowData.getContent(), playerWindowData.getWindowType(), x, y, specialRequest));
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("content", sendData);
		json.put("window", windowType);
		json.put("tile_x", tile_x);
		json.put("tile_y", tile_y);
		json.put("request", request);

		return json;
	}
}
