package me.ienze.Mech.connection;

import me.ienze.Mech.inventory.Inventory;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet006Inventory extends Packet {

	private Inventory inv;
	private long time;

	public Packet006Inventory(Player player) {
		super(player);
	}

	public Packet006Inventory(Inventory inv, long time) {
		super(null);
		this.inv = inv;
		this.time = time;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("id", inv.getId());
		json.put("size", inv.getSize(time));
		json.put("items", inv.getItemsJson(time));

		System.out.println("Sending inv: " + json.toJSONString());

		return json;
	}
}
