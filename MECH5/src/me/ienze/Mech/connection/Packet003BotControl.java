package me.ienze.Mech.connection;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet003BotControl extends Packet {

	public Packet003BotControl(Player player) {
		super(player);
	}

	@Override
	public void onRecive(JSONObject data) {

		Integer id = null, task = 0;
		try {
			id = Integer.valueOf((String) data.get("id"));
			task = Integer.valueOf((String) data.get("task"));
		} catch (NumberFormatException e) {

		}

		if (id != null) {
			Bot bot = Mech.getBotManager().getBot(id);
			if (bot.getOwner(Mech.getActualTime()).equals(this.player)) {

				switch (task) {
				case 1:
					//go
					Integer x = Integer.valueOf((String) data.get("x"));
					Integer y = Integer.valueOf((String) data.get("y"));

					if (x != null && y != null) {
						bot.addTaskGo(x, y);
					}
					break;

				case 2:
					//mine
					bot.addTaskMine();
					break;
					
				case 3:
					//shoot
					x = Integer.valueOf((String) data.get("x"));
					y = Integer.valueOf((String) data.get("y"));

					if (x != null && y != null) {
						bot.addTaskShoot(x, y);
					}
					break;

				default:
					break;
				}

			} else {
				System.out.println("Player " + player.getName() + " is trying to control bot (" + bot.getId() + ") of someone else ("
						+ bot.getOwner(Mech.getActualTime()) + ")!");
			}
		}
	}
}
