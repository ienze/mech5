package me.ienze.Mech.connection;

import java.util.Iterator;

import me.ienze.Mech.player.Player;

public class ConnectionTimeOutWatcher extends Thread {

	private MechServer mechServer;

	public ConnectionTimeOutWatcher(MechServer mechServer) {
		super("ConnectionTimeOutWatcher");

		this.mechServer = mechServer;
	}

	@Override
	public void run() {
		while (true) {

			long minTime = System.currentTimeMillis() - 30 * 1000;

			Iterator<Player> sessionsIter = mechServer.getSessions().values().iterator();

			while (sessionsIter.hasNext()) {
				Player p = sessionsIter.next();

				if (p.getLastPacketUpdate() < minTime) {
					mechServer.closeSession(p);
				}
			}

			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
