package me.ienze.Mech.connection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import me.ienze.Mech.connection.Packet.PacketList;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MechServerConnection extends Thread {

	private static final int max_query_length = 36000;

	private MechServer server;

	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;

	private ArrayList<Packet> rawPackets = new ArrayList<Packet>();

	private Player player;

	public MechServerConnection(MechServer server, Socket socket) {
		super("MechServerConnection");

		this.server = server;

		this.socket = socket;

		try {
			this.in = new DataInputStream(socket.getInputStream());
			this.out = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			System.out.println("connection established " + socket.getInetAddress().getHostName());

			Integer requestLength = in.readInt();

			byte[] bytes = new byte[requestLength];
			in.read(bytes);
			String request = new String(bytes);

			JSONObject requestJson = null;

			// parse string to json
			if (request != null && !request.isEmpty()) {
				JSONParser jsonParser = new JSONParser();
				try {
					requestJson = (JSONObject) jsonParser.parse(request);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			// find player
			String session = (String) requestJson.get("session");
			player = server.getSession(session);

			boolean requireLogin = false;
			if (player == null) {
				requireLogin = true;
			}

			// read request packet
			Integer packet = null;
			if (requestJson != null) {
				Long packetLong = (Long) requestJson.get("packet");
				packet = packetLong.intValue();
			}

			if (packet != null) {
				if (requireLogin && packet != Packet.PacketList.LOGIN.getId()) {
					incorrectSession();
				} else {
					try {
						packetRecived(packet, requestJson, player);
					} catch (Exception e) {
						System.out.println("Problem with recived packet ("+requestJson.toJSONString()+")");
						e.printStackTrace();
					}
					server.onRecivePacket();
				}
			}

			// output
			JSONArray output = new JSONArray();
			int outputLength = 16;

			ArrayList<Packet> packetsToSend = new ArrayList<Packet>();

			if (player != null) {
				packetsToSend = player.getPacketsSorted();

				player.onPacketUpdate();
			}

			int packetLenght = 0;
			for (int i = 0; i < packetsToSend.size(); i++) {
				Packet packetToSend = packetsToSend.get(i);

				String packetString = packetToSend.toSend().toJSONString();
				packetLenght = packetString.length();
				
				if (outputLength + packetLenght < max_query_length) {
					packetsToSend.remove(packetToSend);

					outputLength += packetLenght;

					output.add(packetToSend.toSend());
					server.onSendPacket();
				}
			}
			
			outputLength += packetLenght;
			
			for (Packet rawPacket : rawPackets) {
				output.add(rawPacket.toSend());
				server.onSendPacket();
			}

			if (player != null) {
				player.getPackets().clear();
				player.setPackets(packetsToSend);
			}

			// send packets
			out.writeUTF(output.toJSONString());

			out.write('\r');

			System.out.println("packets sended (" + outputLength + ")");

			// close
			in.close();
			out.close();
			socket.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void incorrectSession() {
		System.out.println("Incorrect session!");
	}

	private void packetRecived(Integer packet, JSONObject requestJson, Player player) {
		PacketList packetType = Packet.PacketList.byId(packet);
		if (packetType != null) {
			Packet p = packetType.newPacket(player);
			p.onRecive(requestJson);

			Packet hardPacket = p.sendHardcodedPacketOnRecive();
			if(hardPacket != null) {
				sendRawPacket(hardPacket);
			}

		} else {
			System.out.println("Recived incorrect packet id!");
		}
	}

	private void sendRawPacket(Packet packet) {
		rawPackets.add(packet);
	}
}
