package me.ienze.Mech.connection;

import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet004MoveCam extends Packet {

	private Integer x, y;

	public Packet004MoveCam(Player player) {
		super(player);
	}

	public Packet004MoveCam(int x, int y) {
		super(null);
		this.x = x;
		this.y = y;
	}

	@Override
	public void onRecive(JSONObject data) {
		x = ((Long) data.get("x")).intValue();
		y = ((Long) data.get("y")).intValue();

		player.setMapLoadingCenter(x, y);
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("x", x);
		json.put("y", y);

		return json;
	}
}
