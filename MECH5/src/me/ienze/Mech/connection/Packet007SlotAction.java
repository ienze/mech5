package me.ienze.Mech.connection;

import me.ienze.Mech.inventory.Item;
import me.ienze.Mech.player.Player;

import org.json.simple.JSONObject;

public class Packet007SlotAction extends Packet {

	private String id;
	private int slot;
	private Item item;

	public Packet007SlotAction(Player player) {
		super(player);
	}

	public Packet007SlotAction(String id, int slot, Item item) {
		super(null);
		this.id = id;
		this.slot = slot;
		this.item = item;
	}

	@Override
	public void onRecive(JSONObject data) {

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toSend() {
		JSONObject json = super.toSend();

		json.put("id", id);
		json.put("slot", slot);
		if (item != null) {
			json.put("t", item.getType());
			json.put("s", item.getSubType());
			json.put("a", item.getAmount());
			json.put("null", false);
		} else {
			json.put("null", true);
		}

		return json;
	}
}
