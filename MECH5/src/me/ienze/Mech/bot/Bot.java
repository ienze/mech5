package me.ienze.Mech.bot;

import java.awt.Point;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.botParts.BotParts;
import me.ienze.Mech.botTasks.TaskGo;
import me.ienze.Mech.botTasks.TaskMine;
import me.ienze.Mech.botTasks.TaskShoot;
import me.ienze.Mech.common.Damagable;
import me.ienze.Mech.common.FutureValueUtils;
import me.ienze.Mech.common.LimitedList;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.FutureValueBotParts;
import me.ienze.Mech.data.value.FutureValueInteger;
import me.ienze.Mech.data.value.FutureValueMap;
import me.ienze.Mech.data.value.FutureValuePlayer;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.Player;

public class Bot implements Damagable {

	private int id;
	private FutureValueMap map;
	private FutureValueInteger x, y;
	private FutureValuePlayer owner;
	private LimitedList<BotTask> tasks;
	private FutureValueBotParts parts;
	private FutureValueInteger health;

	public Bot(int id, Map map, int x, int y, Player owner, BotParts parts) {
		this.id = id;
		this.map = new FutureValueMap("bot." + id + ".map", map);
		this.x = new FutureValueInteger("bot." + id + ".x", x);
		this.y = new FutureValueInteger("bot." + id + ".y", y);
		this.owner = new FutureValuePlayer("bot." + id + ".owner", owner);
		this.parts = new FutureValueBotParts("bot." + id + ".parts", parts);
		this.health = new FutureValueInteger("bot." + id + ".health", 1);
		setTasksLimit(parts.getMaxTasks());
	}

	public Bot(int id) {
		this.id = id;
		this.map = new FutureValueMap("bot." + id + ".map");
		this.x = new FutureValueInteger("bot." + id + ".x");
		this.y = new FutureValueInteger("bot." + id + ".y");
		this.owner = new FutureValuePlayer("bot." + id + ".owner");
		this.parts = new FutureValueBotParts("bot." + id + ".parts", new BotParts(0));
		this.health = new FutureValueInteger("bot." + id + ".health");
		setTasksLimit(parts.getValue(Mech.getActualTime()).getMaxTasks());
	}

	public int getId() {
		return id;
	}

	private void setTasksLimit(int maxTasks) {
		LimitedList<BotTask> oldTasks = tasks;

		this.tasks = new LimitedList<BotTask>(maxTasks);

		if (oldTasks != null) {
			for (BotTask t : oldTasks) {
				addTask(t);
			}
		}
	}

	public Map getMap(long time) {
		return map.getValue(time);
	}

	public FutureValue<Map> getMapVal() {
		return map;
	}

	public int getX(long time) {
		return x.getValue(time);
	}

	public FutureValue<Integer> getXVal() {
		return x;
	}

	public int getY(long time) {
		return y.getValue(time);
	}

	public FutureValue<Integer> getYVal() {
		return y;
	}

	public Point getLoc(long time) {
		return new Point(x.getValue(time), y.getValue(time));
	}

	public void teleport(long time, int x, int y) {
		teleport(map.getValue(time), time, x, y);
	}

	public void teleport(Map map, long time, int x, int y) {
		if (this.map.getValue(time) != map) {

			this.map.addValueChange(time, map);

		}
		this.x.addValueChange(time, x);
		;
		this.y.addValueChange(time, y);
		;
	}

	public Player getOwner(long time) {
		return owner.getValue(time);
	}

	public LimitedList<BotTask> getTasks() {
		return tasks;
	}

	public BotParts getParts(long time) {
		return parts.getValue(time);
	}

	public void addTask(BotTask task) {
		this.tasks.add(task);
	}

	public void addTaskGo(Integer x, Integer y) {

		long time = newTaskTime();

		TaskGo t = new TaskGo(this, x, y, (int) getParts(time).getMoveSpeed(), time);

		if (t.isValidTask()) {
			addTask(t);
		}
	}


	public void addTaskMine() {
		long time = newTaskTime();
		TaskMine task = new TaskMine(this, time, 15000, getX(time), getY(time));
		addTask(task);
	}
	
	public void addTaskShoot(Integer x, Integer y) {
		long time = newTaskTime();
		
		BotParts botParts = getParts(time);

		TaskShoot t = new TaskShoot(this, x, y, botParts.getShootTimeBefore(), botParts.getShootTimeAfter(), time);

		if (t.isValidTask()) {
			addTask(t);
		}
	}

	private long newTaskTime() {
		if (tasks.isEmpty()) {
			return Mech.getActualTime();
		}

		long time = 0;
		for (int i = 0; i < tasks.size(); i++) {
			if (tasks.get(i) != null) {
				long t = tasks.get(i).getEndTime();
				if (t > time) {
					time = t;
				}
			}
		}

		if (time == 0) {
			// something went wrong.. but I dont care
			time = Mech.getActualTime();
		}

		return time;
	}

	public boolean haveChanged(long timeFrom, long timeTo) {
		
		if(FutureValueUtils.doesChange(x, timeFrom, timeTo)
				|| FutureValueUtils.doesChange(y, timeFrom, timeTo)
				|| FutureValueUtils.doesChange(health, timeFrom, timeTo)
				|| FutureValueUtils.doesChange(parts, timeFrom, timeTo)) {
			return true;
		}
		
		return false;
	}

	@Override
	public FutureValueInteger getHealth() {
		return health;
	}

	@Override
	public void damage(int damage, long time) {
		health.addValueChange(time, health.getValue(time) - damage);
	}

	public void removeData() {
		map.remove(); x.remove(); y.remove();
		owner.remove(); parts.remove(); health.remove();
	}

	@Override
	public String toString() {
		return "Bot[owner=" + owner + ", x=" + x + ", y=" + y + ", id=" + id + "]";
	}
}
