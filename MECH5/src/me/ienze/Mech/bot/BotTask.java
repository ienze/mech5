package me.ienze.Mech.bot;

public interface BotTask {

	public long getEndTime();

	public Bot getBot();

	public void onCancel();
}
