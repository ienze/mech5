package me.ienze.Mech.bot.botTasks;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.bot.BotTask;
import me.ienze.Mech.data.value.FutureValueItemsList;
import me.ienze.Mech.data.value.FutureValuesChangeList;
import me.ienze.Mech.inventory.EnumItem;
import me.ienze.Mech.inventory.Inventory;
import me.ienze.Mech.inventory.Item;
import me.ienze.Mech.map.Map;

public class TaskMine implements BotTask {

	private Bot bot;
	private long endTime;
	private FutureValuesChangeList event;

	public TaskMine(Bot bot, long startTime, long miningTime, int x, int y) {

		this.endTime = startTime + miningTime;

		Map map = bot.getMap(endTime);

		int[] resources = map.mine(x, y, endTime);

		event = new FutureValuesChangeList();
		for (int i = 0; i < resources.length; i++) {

			Inventory inv = bot.getOwner(endTime).getInventory();

			FutureValueItemsList items = inv.getItems();

			inv.addItem(new Item(EnumItem.RESOURCE.getId(), i, resources[i]), endTime);

			event.addChangeToValue(items, endTime);
		}
	}

	@Override
	public void onCancel() {
		event.rollbackChnages();
	}

	@Override
	public long getEndTime() {
		return endTime;
	}

	@Override
	public Bot getBot() {
		return bot;
	}
}
