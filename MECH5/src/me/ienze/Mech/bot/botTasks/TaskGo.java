package me.ienze.Mech.bot.botTasks;

import java.awt.Point;
import java.util.LinkedList;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.bot.BotTask;
import me.ienze.Mech.common.PathFinder;
import me.ienze.Mech.common.PathFinder.Node;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.FutureValuesChangeList;
import me.ienze.Mech.events.BotMoveEvent;

public class TaskGo implements BotTask {

	private Bot bot;

	private int targetX, targetY;
	private int moveSpeed;

	private boolean isPathFound;
	private long taskEndTime;

	private FutureValuesChangeList path;

	public TaskGo(Bot bot, int targetX, int targetY, int moveSpeed, long startTime) {
		this.bot = bot;
		this.targetX = targetX;
		this.targetY = targetY;
		this.moveSpeed = moveSpeed;
		findPath(startTime);
	}

	private void findPath(long startTime) {

		System.out.println("Finding path!");

		PathFinder pathFinder = new PathFinder();

		Point botLoc = bot.getLoc(startTime);

		LinkedList<Node> foundPath = pathFinder.findPath(bot.getMap(startTime), botLoc, new Point(targetX, targetY), 512, startTime);

		path = new FutureValuesChangeList();

		if (foundPath != null) {

			FutureValue<Integer> bx = bot.getXVal();
			FutureValue<Integer> by = bot.getYVal();

			int i = 0;

			for (Node node : foundPath) {

				long moveTime = startTime + (i * moveSpeed);

				bx.addValueChange(moveTime, node.getLocation().x);
				by.addValueChange(moveTime, node.getLocation().y);
				
				Mech.getEvents().callEvent(new BotMoveEvent(bot, node.getLocation().x, node.getLocation().y, moveTime));

				path.addChangeToValue(bx, moveTime);
				path.addChangeToValue(by, moveTime);
			}

			taskEndTime = startTime + (i * moveSpeed);
			isPathFound = true;

		} else {
			isPathFound = false;
			System.out.println("Cant find any path..");
		}

		System.out.println("Path found!");
	}

	public boolean isValidTask() {
		return isPathFound;
	}

	@Override
	public long getEndTime() {
		return taskEndTime;
	}

	@Override
	public Bot getBot() {
		return bot;
	}

	@Override
	public void onCancel() {
		path.rollbackChnages();

		path = null;
	}
}
