package me.ienze.Mech.bot.botTasks;

import java.awt.Point;

import me.ienze.Mech.Mech;
import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.bot.BotTask;
import me.ienze.Mech.common.Damagable;
import me.ienze.Mech.data.value.FutureValuesChangeList;
import me.ienze.Mech.events.BotShootEvent;

public class TaskShoot implements BotTask {

	private Bot bot;

	private int targetX, targetY;
	private int beforeShootTime;
	private int afterShootTime;

	private long taskShootTime;

	private FutureValuesChangeList shoot;

	public TaskShoot(Bot bot, int targetX, int targetY, int beforeShootTime, int afterShootTime, long startTime) {
		this.bot = bot;
		this.targetX = targetX;
		this.targetY = targetY;
		this.beforeShootTime = beforeShootTime;
		this.afterShootTime = afterShootTime;
		
		shoot(startTime);
	}

	private void shoot(long startTime) {

		taskShootTime = startTime + beforeShootTime;
		
		Point botLoc = bot.getLoc(startTime);
		
		int shootDistance = bot.getParts(taskShootTime).getShootDistance();
		
		if(botLoc.distance(targetX, targetY) > shootDistance) {
			return;
		}
		
		Damagable target = bot.getMap(taskShootTime).getDamagableTarget(targetX, targetY, taskShootTime);
		
		if(target == null) {
			return;
		}
		
		int damage = bot.getParts(taskShootTime).getShootDamage();

		target.damage(damage, taskShootTime);
		
		Mech.getEvents().callEvent(new BotShootEvent(bot, target, damage));

		shoot = new FutureValuesChangeList();
		
		shoot.addChangeToValue(target.getHealth(), taskShootTime);	
	}

	public boolean isValidTask() {
		return shoot != null;
	}

	@Override
	public long getEndTime() {
		return taskShootTime + afterShootTime;
	}

	@Override
	public Bot getBot() {
		return bot;
	}

	@Override
	public void onCancel() {
		shoot.rollbackChnages();

		shoot = null;
	}
}
