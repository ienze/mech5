package me.ienze.Mech.bot;

import java.util.ArrayList;
import java.util.List;

import me.ienze.Mech.bot.botParts.BotParts;
import me.ienze.Mech.data.loader.ValueLoaderDatabase;
import me.ienze.Mech.data.value.SimpleValueInteger;
import me.ienze.Mech.map.Map;
import me.ienze.Mech.player.Player;

public class BotManager {

	private ArrayList<Bot> bots = new ArrayList<Bot>();
	private SimpleValueInteger maxBotId;

	public BotManager(ValueLoaderDatabase valueLoader) {
		this.maxBotId = new SimpleValueInteger("bots.lastId", 0);
	}

	public void init() {
		// load bots
		int maxBot = maxBotId.getValue();
		System.out.println("Loading bots 0-" + maxBot);
		for (int id = 1; id <= maxBot; id++) {
			bots.add(new Bot(id));
		}
	}

	public List<Bot> getBots() {
		return bots;
	}
	
	public ArrayList<Bot> getBotsOnMap(Map map, long time) {
		ArrayList<Bot> bots = new ArrayList<Bot>();
		
		for(Bot bot : this.bots) {
			if(bot.getMap(time) == map) {
				bots.add(bot);
			}
		}
		
		return bots;
	}

	public ArrayList<Bot> getBotsOwnedBy(Player player, long time) {
		ArrayList<Bot> bots = new ArrayList<Bot>();
		
		//TODO add cache..
		
		for(Bot bot : this.getBots()) {
			if(bot.getOwner(time) == player) {
				bots.add(bot);
			}
		}
		
		return bots;
	}

	public Bot getBot(int id) {
		for (Bot bot : bots) {
			if (bot.getId() == id) {
				return bot;
			}
		}

		Bot bot = new Bot(id);
		return bot;
	}

	public void createNewBot(Map map, int x, int y, Player owner) {

		int newId = newBotId();

		Bot bot = new Bot(newId, map, x, y, owner, new BotParts(0));
		bots.add(bot);

		this.maxBotId.setValue(newId);
	}

	protected int newBotId() {
		return maxBotId.getValue() + 1;
	}

	public void removeAllBots() {
		
		for(Bot bot : bots) {
			bot.removeData();
		}
		bots.clear();
		
		maxBotId.setValue(0);
	}
}
