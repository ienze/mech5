package me.ienze.Mech.bot.botParts;


public class BotSlot {

	private int slot;
	private BotSlotType type;
	
	public BotSlot(int slot, BotSlotType type) {
		this.slot = slot;
		this.type = type;
	}
	
	public int getSlot() {
		return slot;
	}
	
	public BotSlotType getType() {
		return type;
	}
	
	public enum BotSlotType {
		
		WHEEL(1), SHIELD(3), CORE(5);
		
		private int id;
		
		private BotSlotType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		static BotSlotType getById(int id) {
			for(BotSlotType type : BotSlotType.values()) {
				if(type.getId() == id) {
					return type;
				}
			}
			return null;
		}
	}
}
