package me.ienze.Mech.bot.botParts.ability;

import java.util.HashMap;

import me.ienze.Mech.bot.Bot;
import me.ienze.Mech.data.value.FutureValue;
import me.ienze.Mech.data.value.FutureValueFloat;
import me.ienze.Mech.data.value.FutureValueInteger;

public class BotAbilityData {

	private static final float BOT_STARTUP_SPEED = 1;
	private static final int BOT_STARTUP_HEALTH = 1;
	private static final int BOT_STARTUP_DAMAGE = 1;
	
	private Bot bot;
	
	private HashMap<BotAbilityType, FutureValue<?>> abilityData = new HashMap<BotAbilityType, FutureValue<?>>();

	public BotAbilityData(Bot bot) {
		this.bot = bot;
		int id = bot.getId();
		
		abilityData.put(BotAbilityType.SPEED, new FutureValueFloat("bot." + id + ".ability.speed", BOT_STARTUP_SPEED));
		abilityData.put(BotAbilityType.HEALTH, new FutureValueInteger("bot." + id + ".ability.health", BOT_STARTUP_HEALTH));
		abilityData.put(BotAbilityType.DAMAGE, new FutureValueInteger("bot." + id + ".ability.damage", BOT_STARTUP_DAMAGE));
	}
	
	public Bot getBot() {
		return bot;
	}

	public Float getSpeed(long time) {
		return (Float) abilityData.get(BotAbilityType.SPEED).getValue(time);
	}
	
	public FutureValueFloat getSpeedVal() {
		return (FutureValueFloat) abilityData.get(BotAbilityType.SPEED);
	}
	
	public Integer getMaxHealth(long time) {
		return (Integer) abilityData.get(BotAbilityType.HEALTH).getValue(time);
	}
	
	public FutureValueInteger getMaxHealthVal() {
		return (FutureValueInteger) abilityData.get(BotAbilityType.HEALTH);
	}
	
	public Integer getDamage(long time) {
		return (Integer) abilityData.get(BotAbilityType.DAMAGE).getValue(time);
	}
	
	public FutureValueInteger getDamageVal() {
		return (FutureValueInteger) abilityData.get(BotAbilityType.DAMAGE);
	}
	
	public enum BotAbilityType {
		SPEED, HEALTH, DAMAGE;
	}
	
	public FutureValue<?> getValueByType(BotAbilityType type) {
		return abilityData.get(type);
	}
}
