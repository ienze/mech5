package me.ienze.Mech.bot.botParts.ability;

import me.ienze.Mech.bot.botParts.ability.BotAbilityData.BotAbilityType;
import me.ienze.Mech.data.value.FutureValue;

public class AbilityNumeric<T extends Number> implements BotPartAbility {

	private T add;
	private boolean multiply;
	private BotAbilityType type;
	
	public AbilityNumeric(T add, boolean multiply, BotAbilityType type) {
		this.add = add;
		this.multiply = multiply;
		this.type = type;
	}
	
	@Override
	public void preApplyModifier(BotAbilityData botAbility, long time) {
		
	}

	public void applyModifier(BotAbilityData botAbility, long time) {
		if(!multiply) {
			try {
				FutureValue<?> value = botAbility.getValueByType(type);
				
				@SuppressWarnings("unchecked")
				FutureValue<Number> valuen = (FutureValue<Number>)value;
				
				valuen.addValueChange(time, (botAbility.getSpeed(time) + add.floatValue()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void postApplyModifier(BotAbilityData botAbility, long time) {
		if(multiply) {
			try {
				FutureValue<?> value = botAbility.getValueByType(type);
				
				@SuppressWarnings("unchecked")
				FutureValue<Number> valuen = (FutureValue<Number>)value;
				
				valuen.addValueChange(time, (botAbility.getSpeed(time) * add.floatValue()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
