package me.ienze.Mech.bot.botParts.ability;

public interface BotPartAbility {

	public void preApplyModifier(BotAbilityData botAbility, long time);
	
	public void applyModifier(BotAbilityData botAbility, long time);
	
	public void postApplyModifier(BotAbilityData botAbility, long time);
}
