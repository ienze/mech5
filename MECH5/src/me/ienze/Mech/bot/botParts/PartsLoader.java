package me.ienze.Mech.bot.botParts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import me.ienze.Mech.common.PluginClassLoader;

public class PartsLoader extends PluginClassLoader<Part> {

	private HashMap<String, Part> parts = new HashMap<String, Part>();
	
	public PartsLoader() {
		
		ArrayList<Part> loadedParts = loadPluginClasses(new File("resources/parts"));
		for (Part loadedQuest : loadedParts) {
			if(!this.parts.containsKey(loadedQuest.getId())) {
				this.parts.put(loadedQuest.getId(), loadedQuest);
			} else {
				System.out.println("Duplicate bot part. Part with id "+loadedQuest.getId()+" is already registred!");
			}
		}
		
		System.out.println(this.parts.size()+" bot parts loaded.");
	}
}
