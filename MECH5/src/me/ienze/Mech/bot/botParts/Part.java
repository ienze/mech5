package me.ienze.Mech.bot.botParts;

import me.ienze.Mech.bot.botParts.ability.BotPartAbility;

public abstract class Part {

	public abstract String getId();

	public abstract boolean applicableBotSlot(BotSlot slot);
	
	public abstract BotPartAbility[] getPartAbilities();
}
