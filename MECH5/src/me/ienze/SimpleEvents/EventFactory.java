package me.ienze.SimpleEvents;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class EventFactory {

	protected HashMap<Object, ArrayList<Method>> listeners = new HashMap<Object, ArrayList<Method>>();

	public void registerEvents(Object object) {

		ArrayList<Method> methods = new ArrayList<Method>();

		for (Method method : object.getClass().getMethods()) {
			if (isEventMethod(method)) {
				methods.add(method);
			}
		}

		if (!methods.isEmpty()) {
			listeners.put(object, methods);
		}
	}

	private boolean isEventMethod(Method method) {
		EventHandler methodAnnotation = method.getAnnotation(EventHandler.class);
		if (methodAnnotation != null) {
			if (method.getParameterTypes().length == 1) {
				return true;
			}
		}
		return false;
	}

	public void callEvent(Event event) {
		Iterator<Object> listenersIterator = listeners.keySet().iterator();

		while (listenersIterator.hasNext()) {
			Object obj = listenersIterator.next();

			for (Method method : listeners.get(obj)) {
				if (method.getParameterTypes()[0].isInstance(event)) {
					try {
						method.invoke(obj, event);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
